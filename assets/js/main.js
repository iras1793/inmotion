    $(document).ready(function() {
    
        $("body").css("display", "none");
        $("body").fadeIn(2000);
        
      $("a.transicion").click(function(event){
        event.preventDefault();
        linkDestino = this.href;
        $("body").fadeOut(1000, redireccionarPag);    
      });
        
      function redireccionarPag() {
        window.location = linkDestino;
      }



    });

// menu fixed
  $(window).scroll(function() {    
    posicionarMenu();

  function posicionarMenu() {
      var altura_del_header = $('#header').outerHeight(true);

      if ($(window).scrollTop() >= altura_del_header){
          $('#header').addClass('fixed');
      } else {
          $('#header').removeClass('fixed');
      }
  }
});


// MENU DISPLAY
$(".menu-collapsed").click(function() {
  $(this).toggleClass("menu-expanded");
  $('.overlay').toggleClass('active');
});



//parallax_paginasinternas
  
  $(".parallaxinterior-container").mousemove(function(e) {
    
    let screenWidth = $(window).width();
    let screenHeight = $(window).height();
    
    $(".parallaxinterior").css({
      transform:
        "translate(-" + e.pageX/screenWidth * 20 + "px, -" + e.pageY/screenHeight * 20 + "px)"
    });
    
    $(".parallaxinterior .title").css({transform:
      "translate(" + e.pageX/screenWidth * 45 + "px, " + e.pageY/screenHeight * 45 + "px)"
    });
  });

  
//slideshow imagenes páginas internas

var counter = 0, // to keep track of current slide
    $items = $('.diy-slideshow figure'), // a collection of all of the slides, caching for performance
    numItems = $items.length; // total number of slides

// this function is what cycles the slides, showing the next or previous slide and hiding all the others
var showCurrent = function(){
    var itemToShow = Math.abs(counter%numItems);// uses remainder (aka modulo) operator to get the actual index of the element to show  
   
  $items.removeClass('show'); // remove .show from whichever element currently has it
  $items.eq(itemToShow).addClass('show');    
};

// add click events to prev & next buttons 
$('.next').on('click', function(){
    counter++;
    showCurrent(); 
});
$('.prev').on('click', function(){
    counter--;
    showCurrent(); 
});

// if touch events are supported then add swipe interactions using TouchSwipe https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
if('ontouchstart' in window){
  $('.diy-slideshow').swipe({
    swipeLeft:function() {
      counter++;
      showCurrent(); 
    },
    swipeRight:function() {
      counter--;
      showCurrent(); 
    }
  });
}



// if( window.location.href == 'localhost/branding' ){
//       $("#mitexto").text("Branding :D");
// }

