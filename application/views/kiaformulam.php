
            <div class="portada" id="headerformulam"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">KIA</h1>
                  <h4 class="subtitulodos txtgray">Fórmula M</h4>
                  <p class="txt2 txtgray">Un evento pensado para las mujeres que gustan del
mundo del motor, realizado en el Autódromo
Hermanos Rodríguez.
                  </p>
                  <p class="txt2 txtgray">Donde montamos el espacio KIA, con clínica de
cambio de llantas, área lounge y prueba de manejo,
superando las expectativas de participación
esperadas.</p>
                   
              </div>
            </section>

            <div class="video">
              <div class="border">
                 <video controls muted preload poster="<?php echo base_url("assets/img/portada_videoformulam.jpg") ?>"><source src="<?php echo base_url("assets/videos/kia-m.mp4") ?>"></video>
              </div>
            </div>

            <section class="section"></section>

            <div class="galeria">
               <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_2.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_7.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_8.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_9.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_10.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_11.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_12.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_13.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_14.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_15.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_16.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_17.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_formulam/KIA-FORMULA-M_18.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

    <div class="enlaceback"><a href="<?php echo site_url("inicio/experientialmk")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Experiential MKT </span></p></a>   <hr> </div>