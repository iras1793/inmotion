
            <div class="portada" id="headerlgshowroom"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">LG</h1>
                  <h4 class="subtitulodos txtgray">Showroom Business Solutions</h4>
                  <p class="txt2 txtgray">Tomamos un espacio y lo convertimos en un catálogo
vivo, capaz de transmitir los alcances y beneficios
que LG ofrece a sus clientes potenciales.
                  </p>
                  <p class="txt2 txtgray">Generamos el ambiente ideal para ofrecer un
excelente servicio abriendo nuevas oportunidades de
negocio.</p>
                   
              </div>
            </section>

            <!-- <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM1.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM2.jpg") ?>">
                        </li>
                         <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM3.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM4.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM5.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM6.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM7.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM8.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM9.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/lg_showroom/LG_SHOWROOM10.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
