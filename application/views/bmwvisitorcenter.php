
            <div class="portada" id="headervisitorcenter"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">BMW</h1>
                  <h4 class="subtitulodos txtgray">Visitor center y accesos principales planta</h4>
                  <p class="txt2 txtgray">Manejamos la importación y venta del mobiliario y
señalética de toda la red de concesionarios BMW,
Minicooper y Motorrad.
                  </p>
                  <p class="txt2 txtgray">También estamos a cargo de la operación del
montaje de estos.</p>
                   <p class="txt2 txtgray">Brindando un servicio de excelencia en toda la
república mexicana lo que nos caracteriza como
socios comerciales con BMW México.
                  </p>
              </div>
            </section>

            <!-- <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div>
 -->
            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_visitorcenter/Visitor Center Planta BMW.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_visitorcenter/Visitor Center Planta BMW_01.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_visitorcenter/Visitor Center Planta BMW_02.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
