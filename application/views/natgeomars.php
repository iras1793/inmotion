
            <div class="portada" id="headernatgeomars"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">National Geographic</h1>
                <h4 class="subtitulodos txtgray">Mars - Museo de Historia Natural</h4>
                  <p class="txt2 txtgray">Montamos “A lo marciano. Un vistazo al planeta rojo”
Inspirada en la serie “Mars” de National Geographic.
                  </p>
                  <p class="txt2 txtgray">Una exposición temporal en el Museo de Historia
Natural con información acerca de las últimas
investigaciones científicas realizadas de Marte que
permitió experimentar, las posibles condiciones de
vida que se podrían tener en ese planeta.</p>
                   
              </div>
            </section>

            <div class="video">
              <div class="border">
                 <video controls muted preload poster="<?php echo base_url("assets/img/portada_videomars.jpg") ?>"><source src="<?php echo base_url("assets/videos/natgeo_mars.mp4") ?>"></video>
              </div>
            </div>

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_1.jpg") ?>">
                        </li>
                         <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_4.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_6.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_7.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_8.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_9.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/mars/NATGEO_MARS_10.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
