<div id="parallax-maincontainer">

	<section>

		<div class="slider">
			<div class="textoslider"><p>Keep <span class="txtorange">on'</span> moving </p></div>
			<ul class="cb-slideshow">
				<li><img src="<?php echo base_url("assets/img/slider1.jpg") ?>"></li>
				<li><img src="<?php echo base_url("assets/img/slider2.jpg") ?>"></li>
				<li><img src="<?php echo base_url("assets/img/slider3.jpg") ?>"></li>
				<li><img src="<?php echo base_url("assets/img/slider4.jpg") ?>"></li>
				<li><img src="<?php echo base_url("assets/img/slider5.jpg") ?>"></li>
			</ul>
		</div>
	</section>

	<section id="services">
		<div class="parallax-one">
			<div class="contenido_parallax ancla" >
				<h1 class="content__title">Our <span class="txtgray">services</span></h1>
				<ul class="listaservicios">
					<li><a href="<?php echo site_url("inicio/events")  ?>">
						<div class="itemscontent">
							<!-- <img src="img/events.svg" class="icono"> -->
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="icono">
							<style type="text/css">
								.st0{fill:#585859;}
							</style>
							<g>
								<g>
									<path class="st0" d="M452,40h-24V0h-40v40H124V0H84v40H60C26.9,40,0,66.9,0,100v352c0,33.1,26.9,60,60,60h392
									c33.1,0,60-26.9,60-60V100C512,66.9,485.1,40,452,40z M472,452c0,11-9,20-20,20H60c-11,0-20-9-20-20V188h432V452z M472,148H40v-48
									c0-11,9-20,20-20h24v40h40V80h264v40h40V80h24c11,0,20,9,20,20V148z"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="76" y="230" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="156" y="230" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="236" y="230" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="316" y="230" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="396" y="230" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="76" y="310" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="156" y="310" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="236" y="310" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="316" y="310" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="76" y="390" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="156" y="390" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="236" y="390" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="316" y="390" class="st0" width="40" height="40"/>
								</g>
							</g>
							<g>
								<g>
									<rect x="396" y="310" class="st0" width="40" height="40"/>
								</g>
							</g>
						</svg>

						<p class="nombreservicio">Events</p>
					</div>
				</a>
			</li>
			<li><a href="<?php echo site_url("inicio/experimental")  ?>">
				<div class="itemscontent">
					<!-- <img src="img/experiential-marketing.svg" class="icono"> -->
					<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="icono">
					<style type="text/css">
						.st0{fill:#585859;}
					</style>
					<g id="surface1">
						<path class="st0" d="M256,0C168,0,96.4,71.6,96.4,159.6c0,50.6,23.9,97.9,64.3,128v13.3c-9.8,8.6-16.1,21.3-16.1,35.4
						c0,12.4,4.8,23.7,12.7,32.1c-7.9,8.4-12.7,19.7-12.7,32.1c0,21.1,14,39.1,33.2,45C184,483.2,216.7,512,256,512s72-28.8,78.2-66.4
						c19.2-6,33.2-23.9,33.2-45c0-12.4-4.8-23.7-12.7-32.1c7.9-8.4,12.7-19.7,12.7-32.1c0-14.1-6.2-26.8-16.1-35.4v-13.3
						c40.4-30.1,64.3-77.4,64.3-128C415.6,71.6,344,0,256,0z M320.3,319.2c9.4,0,17.1,7.7,17.1,17.1s-7.7,17.1-17.1,17.1H191.7
						c-9.4,0-17.1-7.7-17.1-17.1s7.7-17.1,17.1-17.1H320.3z M256,482c-21.9,0-40.6-14.4-46.9-34.3H303C296.6,467.6,277.9,482,256,482z
						M320.3,417.7H191.7c-9.4,0-17.1-7.7-17.1-17.1s7.7-17.1,17.1-17.1h128.5c9.4,0,17.1,7.7,17.1,17.1
						C337.4,410,329.7,417.7,320.3,417.7z M328,267.4c-4.2,2.8-6.7,7.5-6.7,12.5v9.4c-0.4,0-0.7,0-1.1,0h-17.1v-59.1l21.5-21.5
						c5.9-5.9,5.9-15.4,0-21.2c-5.9-5.9-15.4-5.9-21.2,0l-25.9,25.9c-2.8,2.8-4.4,6.6-4.4,10.6v65.3h-34.3V224c0-4-1.6-7.8-4.4-10.6
						l-25.9-25.9c-5.9-5.9-15.4-5.9-21.2,0c-5.9,5.9-5.9,15.4,0,21.2l21.5,21.5v59.1h-17.1c-0.4,0-0.7,0-1.1,0v-9.4
						c0-5-2.5-9.7-6.7-12.5c-36.1-24.1-57.6-64.4-57.6-107.8C126.4,88.1,184.5,30,256,30s129.6,58.1,129.6,129.6
						C385.6,202.9,364.1,243.2,328,267.4z"/>
						<path class="st0" d="M62.1,159.6c0-8.3-6.7-15-15-15H15c-8.3,0-15,6.7-15,15s6.7,15,15,15h32.1C55.4,174.6,62.1,167.9,62.1,159.6z"
						/>
						<path class="st0" d="M497,144.6h-32.1c-8.3,0-15,6.7-15,15s6.7,15,15,15H497c8.3,0,15-6.7,15-15S505.3,144.6,497,144.6z"/>
						<path class="st0" d="M67.6,251l-27.8,16.1c-7.2,4.1-9.6,13.3-5.5,20.5c2.8,4.8,7.8,7.5,13,7.5c2.5,0,5.1-0.6,7.5-2L82.6,277
						c7.2-4.1,9.6-13.3,5.5-20.5S74.8,246.9,67.6,251z"/>
						<path class="st0" d="M436.9,70.2c2.5,0,5.1-0.6,7.5-2l27.8-16.1c7.2-4.1,9.6-13.3,5.5-20.5s-13.3-9.6-20.5-5.5l-27.8,16.1
						c-7.2,4.1-9.6,13.3-5.5,20.5C426.7,67.5,431.7,70.2,436.9,70.2z"/>
						<path class="st0" d="M472.2,267.1L444.4,251c-7.2-4.1-16.4-1.7-20.5,5.5c-4.1,7.2-1.7,16.3,5.5,20.5l27.8,16.1c2.4,1.4,4.9,2,7.5,2
						c5.2,0,10.2-2.7,13-7.5C481.8,280.4,479.4,271.3,472.2,267.1z"/>
						<path class="st0" d="M82.6,42.2L54.8,26.1c-7.2-4.1-16.3-1.7-20.5,5.5c-4.1,7.2-1.7,16.3,5.5,20.5l27.8,16.1c2.4,1.4,4.9,2,7.5,2
						c5.2,0,10.2-2.7,13-7.5C92.2,55.5,89.8,46.3,82.6,42.2z"/>
					</g>
				</svg>

				<p class="nombreservicio">Experiential Marketing</p>
			</div>
		</a>
	</li>
	<li><a href="<?php echo site_url("inicio/custom")  ?>">
		<div class="itemscontent">
			<!-- <img src="img/custom.svg" class="icono"> -->
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="icono">
			<style type="text/css">
				.st0{fill:#585859;}
			</style>
			<path class="st0" d="M180,60h61v30h-61V60z"/>
			<path class="st0" d="M180,120h61v30h-61V120z"/>
			<path class="st0" d="M60,466c0,24.9,20.1,46,45,46h151c24.9,0,45-21.1,45-46v-30c0-8.3,6.7-15,15-15s15,6.7,15,15v30
			c0,24.9,20.1,46,45,46h136v-30H376c-8,0-15-7.5-15-16v-30c0-24.8-20.2-45-45-45s-45,20.2-45,45v30c0,8.5-7,16-15,16H105
			c-8,0-15-7.5-15-16v-15h151V301h-91v-30h112.2l-25.6-25.6c-9.6-9.6-16.9-21.9-21.2-35.4h74.2l60-30H376c36.2,0,66.5-25.8,73.5-60
			H512V90h-62.5c-7-34.2-37.3-60-73.5-60h-26.5l-60-30H0v210h30v91H0v150h60V466z M196.2,241H150v-31h34.2
			C186.8,220.9,190.8,231.4,196.2,241L196.2,241z M421,105c0,24.8-20.2,45-45,45h-15V60h15C400.8,60,421,80.2,421,105z M331,155.7
			l-30,15V39.3l30,15V155.7z M30,30h241v150H30V30z M120,210v121h91v30h-99.8L60,309.8V210H120z M30,331h8.8l60,60H211v30H30V331z"/>
			<path class="st0" d="M105,60c-24.8,0-45,20.2-45,45s20.2,45,45,45s45-20.2,45-45S129.8,60,105,60z M105,120c-8.3,0-15-6.7-15-15
			s6.7-15,15-15s15,6.7,15,15S113.3,120,105,120z"/>
		</svg>
		<p class="nombreservicio">Custom, stands and display</p>
	</div>
</a>
</li>
<li><a href="<?php echo site_url("inicio/branding")  ?>">
	<div class="itemscontent">
		<!-- <img src="img/branding.svg" class="icono"> -->
		<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" class="icono">
		<style type="text/css">
			.st0{fill:#585859;}
			.st1{fill:#585859;}
		</style>
		<path class="st0" d="M113.1,0C50.8,0,0,50.8,0,113.1V512h226.3V386.3l87.2,87.2l160-160l-87.2-87.2H512V0H113.1z M482,30v166.2
		h-71.4V30H482z M279.1,30v89.1l-87.7-87.7h-0.1c-0.5-0.5-1.1-1-1.6-1.5L279.1,30z M196.2,309.1v71.4H30v-71.4H196.2z M30,279.1
		v-71.4h166.2v71.4H30z M226.2,108.7l61.3,61.3l-61.2,61.4V113.1C226.3,111.6,226.2,110.2,226.2,108.7z M113.1,30
		c45.8,0,83.1,37.3,83.1,83.1v64.5H30v-64.5C30,67.3,67.3,30,113.1,30z M30,482v-71.4h166.2V482H30z M226.3,273.9l82.5-82.7
		l50.5,50.5L241.9,359.4l-15.6-15.6L226.3,273.9z M313.5,431l-50.4-50.4l117.3-117.7l50.6,50.6L313.5,431z M309.1,149.2V30h71.4
		v166.2h-24.3L309.1,149.2z"/>
		<path class="st1" d="M113.1,117.9c7.9,0,15.4-6.9,15-15c-0.4-8.1-6.6-15-15-15c-7.9,0-15.4,6.9-15,15
		C98.5,111.1,104.7,117.9,113.1,117.9z"/>
	</svg>
	<p class="nombreservicio">Branding</p>
</div>
</a>
</li>
</ul>
</div>
</div>
</section>

<section id="clients">
	<div class="block">
		<div class="contenido_parallax ancla">
			<p class="content__title txtgray titulomobile">Our <span class="txtorange">clients</span></p>
			<ul class="grid">
				<?php if (is_array($clientes)): ?>
					<?php foreach ($clientes as $key => $data): ?>
						<?php $imagen_cliente = $data->imagen_cliente; ?>
						<li>
							<img src="<?php echo base_url("assets/gallery/clientes/$imagen_cliente") ?>">
						</li>
					<?php endforeach ?>
				<?php endif ?>
				<!--
				<li>
					<a href="<?php echo site_url("inicio/equsport")  ?>">
						<img src="<?php echo base_url("assets/img/clients_balvanera.svg") ?>">
					</a>
				</li>
				<li>
					<img src="<?php echo base_url("assets/img/clients_banjercito.svg") ?>">
				</li>
				<li>
					<a href="<?php echo site_url("inicio/bmwvisitorcenter")  ?>">
						<img src="<?php echo base_url("assets/img/clients_bmw.svg") ?>">
					</a>
				</li>
				<li>
					<img src="<?php echo base_url("assets/img/clients_carrier.svg") ?>">
				</li>
				<li>
					<img src="<?php echo base_url("assets/img/clients_cat.svg") ?>">
				</li>
				<li>
					<img src="<?php echo base_url("assets/img/clients_celgene.svg") ?>">
				</li>
				<li><a href="<?php echo site_url("inicio/longines")  ?>"><img src="<?php echo base_url("assets/img/clients_foxsports.svg") ?>"></a></li>
				<li><a href="<?php echo site_url("inicio/fox")  ?>"><img src="<?php echo base_url("assets/img/clients_fox.svg") ?>"></a></li>
				<li><img src="<?php echo base_url("assets/img/clients_innocean.svg") ?>"></a></li>
				<li><a href="<?php echo site_url("inicio/kiaformulam")  ?>"><img src="<?php echo base_url("assets/img/clients_kia.svg") ?>"></a></li>
				<li><a href="<?php echo site_url("inicio/lgshowroom")  ?>"><img src="<?php echo base_url("assets/img/clients_lg.svg") ?>"></a></li>
					<li><img src="<?php echo base_url("assets/img/clients_liverpool.svg") ?>"></li>
					<li><a href="<?php echo site_url("inicio/bmwmini")  ?>"><img src="<?php echo base_url("assets/img/clients_mini.svg") ?>"></a></li>
					<li><a href="<?php echo site_url("inicio/minisoshowroom")  ?>"><img src="<?php echo base_url("assets/img/clients_miniso.svg") ?>"></a></li>
					<li><a href="<?php echo site_url("inicio/mobil")  ?>"><img src="<?php echo base_url("assets/img/clients_mobil.svg") ?>"></a></li>
					<li><img src="<?php echo base_url("assets/img/clients_mtv.svg") ?>"></li>
					<li><a href="<?php echo site_url("inicio/natgeokids")  ?>"><img src="<?php echo base_url("assets/img/clients_natgeo.svg") ?>"></a></li>
					<li><img src="<?php echo base_url("assets/img/clients_nba.svg") ?>"></li>
					<li><img src="<?php echo base_url("assets/img/clients_nickelodeon.svg") ?>"></li>
					<li><a href="<?php echo site_url("inicio/toryburch")  ?>"><img src="<?php echo base_url("assets/img/clients_palaciohierro.svg") ?>"></a></li>
					<li><img src="<?php echo base_url("assets/img/clients_redbull.svg") ?>"></li>
					<li><img src="<?php echo base_url("assets/img/clients_rich.svg") ?>"></li>
					<li><img src="<?php echo base_url("assets/img/clients_sedena.svg") ?>"></li>
					<li><a href="<?php echo site_url("inicio/summit")  ?>"><img src="<?php echo base_url("assets/img/clients_summit.svg") ?>"></a></li>
					<li><a href="<?php echo site_url("inicio/taboola")  ?>"><img src="<?php echo base_url("assets/img/clients_taboola.svg") ?>"></a></li>
					<li><a href="<?php echo site_url("inicio/toryburch")  ?>"><img src="<?php echo base_url("assets/img/clients_toryburch.svg") ?>"></a></li>
					<li><a href="<?php echo site_url("inicio/toyotalaliga")  ?>"><img src="<?php echo base_url("assets/img/clients_toyota.svg") ?>"></a></li>
					<li><img src="<?php echo base_url("assets/img/clients_viacom.svg") ?>"></li>
					<li><img src="<?php echo base_url("assets/img/clients_wilsey.svg") ?>"></li>
				-->
				</ul>
				<p class="content__title txtgray titledesk">Our <span class="txtorange">clients</span></p>
			</div>  
		</div>
	</section>

	<section id="work">
		<div class="parallax-two ancla">
			<div class="content__inner" id="paneles">
				<p class="content__title">Our <span class="txtorange">work</span></p>
				<div class="wrapper">
					<?php if (is_array($casos_exito)): ?>
						<?php foreach ($casos_exito as $key => $data): ?>
							<?php 
							$imagen  = $data->imagen_fondo;
							$slug    = $data->slug;
							$seccion = '';
							switch ($data->seccion):
								case 'Events':
									$seccion = 'events';
								break;
								case 'ExperimentalMarketing':
									$seccion = 'experimental';
								break;
								case 'Custom':
									$seccion = 'custom';
								break;
								case 'Branding':
									$seccion = 'branding';
								break;
								default:
									# code...
									break;
							endswitch;
							?>
							<div id="1" class="section">
								<a href="<?php echo site_url("inicio/$seccion/$slug")  ?>">
								<img src="<?php echo base_url("assets/gallery/nuevas/$imagen") ?>">
								<article class="content_wrapper">
										<p class="nombreseccion"><?php echo $data->titulo ?></p>
										<p class="titulo"><?php echo $data->subtitulo ?></p>
								</article>
								</a>
							</div>
						<?php endforeach ?>
					<?php endif ?>
				<!--
					<div id="2" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-2.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/events/tory-burch-primavera-verano-2019")  ?>">
								<p class="nombreseccion">Events</p>
								<p class="titulo"> Tory Burch:<br>Primavera-Verano 2019 </p>
							</a>
						</article>
					</div>
					<div id="3" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-3.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/experimental/natgeo-kids-natgeo-lab-raro-pero-cierto")  ?>">
								<p class="nombreseccion"> Experiential Marketing </p>
								<p class="titulo"> Nat Geo Kids:<br>Nat Geo Lab, Raro pero cierto </p>
							</a>
						</article>                   
					</div>
					<div id="4" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-4.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/custom/miniso-showroom-terraza")  ?>">
								<p class="nombreseccion"> Custom </p>
								<p class="titulo"> Miniso: Showroom - Terraza </p>
							</a>
						</article>
					</div>
					<div id="5" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-5.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/branding/fox-networks-group-oficinas-fox")  ?>">
								<p class="nombreseccion"> Branding </p>
								<p class="titulo"> FOX: Branding oficinas </p>
							</a>
						</article>
					</div>
					<div id="6" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-6.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/events/sports-anti-piracy-summit-2019")  ?>">
								<p class="nombreseccion">Events</p>
								<p class="titulo">Sports Summit - 2019 </p>
							</a>
						</article>
					</div>
					<div id="7" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-7.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/custom/bmw-training-center-planta")  ?>">
								<p class="nombreseccion">Custom</p>
								<p class="titulo">BMW - Training Center </p>
							</a>
						</article>
					</div>
					<div id="8" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-8.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/experimental/kia-formula-m")  ?>">
								<p class="nombreseccion"> Experiential Marketing </p>
								<p class="titulo"> KIA - Fórmula M</p>
							</a>
						</article>                   
					</div>
					<div id="9" class="section">
						<a href="<?php echo site_url("inicio/experimental/fox-sports-longines-global-champions-tour")  ?>">
						<img src="<?php echo base_url("assets/img/seccion4-9.jpg") ?>">
						<article class="content_wrapper">
								<p class="nombreseccion">Experiential Marketing </p>
								<p class="titulo">Fox Sports <br> Longines Global Champions Tour</p>
						</article>
						</a>
					</div>
					<div id="10" class="section">
						<img src="<?php echo base_url("assets/img/seccion4-10.jpg") ?>">
						<article class="content_wrapper">
							<a href="<?php echo site_url("inicio/experimental/mobil-activaciones-tornado-de-la-suerte")  ?>">
							
								<p class="nombreseccion">Experiential Marketing </p>
								<p class="titulo">Mobil <br> Activaciones Tornado de la suerte</p>
							</a>
						</article>
					</div>
				-->
				</div>            
			</div>
		</div>
	</section>

	<section id="keyplayers">
		<div class="block">
			<div class="contenido_parallax ancla">
				<p class="content__title txtgray titulomobile">Key <span class="txtorange"> players</span></p>
				<div class="imagekeyplayers">

					<ul id="contentimages">
						<?php if (is_array($keyplayers)): ?>
							<?php foreach ($keyplayers as $key => $data): ?>
								<?php $imagen_keyplayer = $data->imagen_keyplayer; ?>
								<li class="player" id="andrea">
									<img src="<?php echo base_url("assets/gallery/keyplayers/$imagen_keyplayer") ?>"  class="imagegray">
									<div class="infoplayer" id="infoandrea">
										<a href="<?php echo $data->linkedin ?>" class="socialplayer" target="_blank">
											<p class="nameplayer"><?php echo $data->nombre ?></p>
											<p class="charge"><?php echo $data->cargo ?></p>
											<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
										</a>
									</div>
								</li>
							<?php endforeach ?>
						<?php endif ?>

<!--
						<li class="player" id="piti">
							<img src="<?php echo base_url("assets/img/Piti Licona Producer_.jpg") ?>" class="imagegray">
							<div class="infoplayer" id="infopiti">
								<a href="http://linkedin.com/in/ana-isabel-licona-grotewold-62619512a" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Piti Licona</p>
									<p class="charge">Producer</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li div class="player" id="anaid">
							<img src="<?php echo base_url("assets/img/Anaid García Digital manager_.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infoanaid">
								<a href="http://linkedin.com/in/anaid-garcía-hernández-99611779" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Anaid García</p>
									<p class="charge">Digital manager</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>                                       
						</li>

						<li class="player" id="edgar">
							<img src="<?php echo base_url("assets/img/Edgar Paredes Producer_.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infoedgar">
								<a href="http://linkedin.com/in/edgar-e-paredes-coronado-87227a107" class="socialplayer" target="_blank">
									<p class="nameplayer">Edgar Paredes</p>
									<p class="charge">Producer</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="alvaro">
							<img src="<?php echo base_url("assets/img/Álvaro Sánchez Account project manager_.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infoalvaro">
								<a href="http://linkedin.com/in/alvaro-y-sanchez-limon-04558b54" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Alvaro Sánchez</p>
									<p class="charge">Account project <br>manager</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="claudia">
							<img src="<?php echo base_url("assets/img/Claudia Vargas New mkt business manager.jpg") ?>" class="imagegray">
							<div class="infoplayer" id="infoclaudia">
								<a href="http://linkedin.com/in/chispa-vargas-91b86863" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Claudia Vargas</p>
									<p class="charge">New marketing<br>business manager</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="bracko">
							<img src="<?php echo base_url("assets/img/Bracko Alcalá CEO & Founder.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infobracko">
								<a href="http://linkedin.com/in/braco-alcala-84a7a5113" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Bracko Alcalá</p>
									<p class="charge">CEO&<br>Founder</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="marisa">
							<img src="<?php echo base_url("assets/img/Marisa Galván Mkt business manager_.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infomarisa">
								<a href="http://linkedin.com/in/marisa-galvan-ruiz-54b57a65" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Marisa Galván</p>
									<p class="charge">Marketing business<br>manager</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="ana">
							<img src="<?php echo base_url("assets/img/Ana_Cedillo_Talent_Manager.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infoana">
								<a href="http://linkedin.com/in/ana-cedillo-2261801a" class="socialplayer" target="_blank">
									<p class="nameplayer">Ana Cedillo</p>
									<p class="charge">Talent<br> manager</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>
						<li class="player" id="ximena">
							<img src="<?php echo base_url("assets/img/Ximena Rivera Head of creative exp_.jpg") ?>" class="imagegray">
							<div class="infoplayer" id="infoximena">
								<a href="http://linkedin.com/in/guadalupe-ximena-rivera-garcía-667072149" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Ximena Rivera</p>
									<p class="charge">Head of creative <br> experiential</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="gerardo">
							<img src="<?php echo base_url("assets/img/Gerardo Maldonado Marketing purchasing_.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infogerardo">
								<a href="http://linkedin.com/in/jose-gerardo-maldonado-dominguez-833173106" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Gerardo Maldonado</p>
									<p class="charge">Marketing <br> purchasing</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>

						<li class="player" id="araceli">
							<img src="<?php echo base_url("assets/img/Araceli Díaz Art director.jpg") ?>"  class="imagegray">
							<div class="infoplayer" id="infoaraceli">
								<a href="http://linkedin.com/in/araceli-diaz-72973ab5" class="socialplayer" target="_blank"> 
									<p class="nameplayer">Araceli Díaz</p>
									<p class="charge">Art<br>Director</p>
									<img src="<?php echo base_url("assets/img/linkedin.png") ?>">
								</a>
							</div>
						</li>
-->
					</ul>
				</div>
				<p class="content__title txtgray titledesk">Key <span class="txtorange"> players</span></p>
			</div>  
		</div>
	</section>

	<section id="contact">
		<ul class="block ancla" id="contactform">
			<li class="formulario">
				<div class="contenido_parallax">
					<p class="content__title">Get in <span class="txtorange">touch</span></p>
					<form class="form">                                  
						<input type="email" class="email entry" placeholder="E-mail"/>

						<textarea class="message entry" placeholder="Message"></textarea> 

						<button class="submit entry" onclick="thanks()">Send</button>
					</form>
				</div>  
			</li>
			<li class="contacto">
				<img src="<?php echo base_url("assets/img/contactobg.png") ?>">
				<ul class="datoscontacto">
					<li class="social">
						<div class="redes">
							<a href="https://www.facebook.com/inmotionmktg/" target="_blank" class="redes" id="facebook"><img src="<?php echo base_url("assets/img/facebook.svg") ?>"></a>
							<a href="https://www.instagram.com/inmotionmktg/" target="_blank" class="redes" id="instagram"><img src="<?php echo base_url("assets/img/instagram.svg") ?>"></a>
							<a href="https://www.linkedin.com/company/inmotion-communications" target="_blank" class="redes" id="linkedin"><img src="<?php echo base_url("assets/img/linkedin.svg") ?>"></a>
							<a href="https://vimeo.com/inmotionmktg" target="_blank" class="redes" id="vimeo"><img src="<?php echo base_url("assets/img/vimeo.svg") ?>"></a>
						</div>
					</li>          
					<li class="inline"><span class="iconosocial"><img src="<?php echo base_url("assets/img/email.svg") ?>"></span><a href="mailto:info@inmotion.com.mx">info@inmotion.com.mx</a></li>
					<li class="inline"><span class="iconosocial"><img src="<?php echo base_url("assets/img/phone.svg") ?>"></span><a href="tel:5553938627" class="telefono">Tel. 55 5393 8627</a></li>
					<li class="inline"><span class="iconosocial"><img src="<?php echo base_url("assets/img/pin.svg") ?>"></span><p class="direccion">
						Monte Elbruz 132, Int. 101,
						<br>11850, Col. San Miguel Chapultepec,
						<br>CDMX
					</p>
				</li>

				<li> <div id="map" class="submit entry"><a href="https://goo.gl/maps/YRB7N7QXjNNK8EUA6" target="_blank">View map</a></div></li>
			</ul>                  
		</li>
	</ul>
</section>

</div>
