
            <div class="portada" id="headerequsport"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">Balvanera</h1>
                  <h4 class="subtitulodos txtgray">Equsport Balvanera 2019</h4>
                  <p class="txt2 txtgray">EQUSPORT fue parte del comité organizador del CSI
Internacional, donde se dieron cita todos los clubes y
agrupaciones afiliadas a la FEM, en las instalaciones
de Balvanera Polo & Golf Club. Con una bolsa total de
premios en efectivo $ 7,870,000.00
                  </p>
                 
              </div>
            </section>

            <div class="video">
              <div class="border">
                 <video controls muted preload poster="<?php echo base_url("assets/img/portada_videoequsport.jpg") ?>"><source src="<?php echo base_url("assets/videos/Equm.mp4") ?>"></video>
              </div>
            </div>

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_2.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_3.jpg") ?>"> 
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_7.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_8.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/equsport/EQUSPORT_BALVANERA_9.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

   <div class="enlaceback"><a href="<?php echo site_url("inicio/events")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Events </span></p></a>   <hr>  </div>
