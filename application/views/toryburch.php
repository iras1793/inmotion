
            <div class="portada" id="headertoryburch"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">Tory Burch</h1>
                  <h4 class="subtitulodos txtgray">Primavera - Verano</h4>
                  <p class="txt2 txtgray">Tory Burch celebró su nueva colección con un evento que produjimos dentro de El Palacio De Hierro de Polanco. 
                  </p>
                  <p class="txt2 txtgray">Instalamos un jardín secreto a modo de laberinto con exhuberante vegetación.<br>
                    Cubrimos una pared de follaje y aves tropicales, inspirada en un estampado de esta temporada, que se convirtió en el photo opportunity perfecto.</p>

              </div>
            </section>

            <!-- <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">

                   <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/1-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        <!-- <article class="content_wrapper">
                            <p class="nombreseccion">Evento Imagen 1</p>
                            <p class="titulo"> Taboola: Glow Party </p>
                        </article> -->
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/2-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/3-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/4-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/5-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/6-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/7-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/8-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/9-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/10-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/11-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/12-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     <li>
                        <img src="<?php echo base_url("assets/gallery/toryburch/13-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                     </li>
                     </ul>
                   </div>
            </div>

            <div class="enlaceback"><a href="<?php echo site_url("inicio/events")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Events </span></p></a>   <hr>  </div>
