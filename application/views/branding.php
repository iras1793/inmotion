<!--
            <div class="banner" id="foxoficinas">
              <a href="<?php echo site_url("inicio/fox")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">FOX Networks Group </h1>
                  <h4 class="subtitulo">Oficinas FOX</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
-->    

<?php if (is_array($branding)): ?>
    <?php foreach ($branding as $key => $data): ?>
          <div class="banner" style="background-image: url('<?php echo base_url("assets/gallery/nuevas/$data->imagen_teaser"); ?>')">
              <a href="<?php echo site_url("inicio/events/$data->slug")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"> <?php echo $data->titulo; ?></h1>
                  <h4 class="subtitulo"><?php echo $data->subtitulo; ?></h4>
                </div>
              </a>
          </div>
          <section class="section"></section>
    <?php endforeach ?>
<?php endif ?>


 <div class="enlaceback"><a href="<?php echo site_url("inicio/services")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> our services </span></p></a>   <hr>  </div>