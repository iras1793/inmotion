            <div class="portada" id="headertoryburch"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior">Tory Burch</h1>
                  <h4 class="subtitulodos">Primavera - Verano</h4>
                  <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctu.
                  </p>
                  <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosq.</p>
                   <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctu.
                  </p>
              </div>
            </section>

            <div class="video">
              <div class="border">
                <video src="<?php echo base_url("assets/videos/1.mp4") ?>" controls preload></video>
              </div>
            </div>

            <section class="section"></section>

            <div class="galeria">
              <div class="border">

                  <div class="diy-slideshow">
                        <figure class="show">
                          <img src="<?php echo base_url("assets/gallery/toryburch/1-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/2-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/3-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/4-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/5-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/6-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                         <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/7-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/8-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/9-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/10-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/11-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/12-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <figure>
                          <img src="<?php echo base_url("assets/gallery/toryburch/13-tory_burch_2019_palacio_de_hierro_produccion_evento.jpg") ?>">
                        </figure>
                        <span class="prev">&laquo;</span>
                        <span class="next">&raquo;</span>
                      </div> 

              </div>
            </div>

