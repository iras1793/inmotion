
            <div class="portada" id="headerminisoshowroom"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">Miniso</h1>
                  <h4 class="subtitulodos txtgray">Showroom terraza</h4>
                  <p class="txt2 txtgray">Miniso inauguró su primera flagship store en la
Ciudad de México, un edificio blanco de la Roma con
tres pisos de más de 700 metros.
                  </p>
                  <p class="txt2 txtgray">En el tercer piso tuvimos la misión de diseñar su
“Showroom Terraza”.</p>
                   
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-1.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-2.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-3.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-4.jpg") ?>">
                        </li>
                        <li>
                        <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-5.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-6.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-7.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-8.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-9.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-10.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-11.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-12.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/miniso_showroom/MINISO-SHOWROOM-TERRAZA-12-2.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>

