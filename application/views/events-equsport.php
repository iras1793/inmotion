<?php
  $pag="equsport";
  include ("includes/metas.php");
  include("includes/header.php");
?>

        
        <main>

            <div class="portada" id="headerequsport"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior">Equsport</h1>
                  <h4 class="subtitulodos">Balvanera</h4>
                  <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctu.
                  </p>
                  <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosq.</p>
                   <p class="txt2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctu.
                  </p>
              </div>
            </section>

            <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div>

            <section class="section"></section>

            <div class="galeria">
              <div class="border">

                  <div class="diy-slideshow">
                        <figure class="show">
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_1.jpg">
                        </figure>
                        <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_2.jpg">
                        </figure>
                        <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_3.jpg"> 
                        </figure>
                        <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_4.jpg">
                        </figure>
                        <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_5.jpg">
                        </figure>
                        <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_6.jpg">
                        </figure>
                         <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_7.jpg">
                        </figure>
                         <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_8.jpg">
                        </figure>
                         <figure>
                          <img src="gallery/equsport/EQUSPORT_BALVANERA_9.jpg">
                        </figure>
                        <span class="prev">&laquo;</span>
                        <span class="next">&raquo;</span>
                      </div> 

              </div>
            </div>

        </main>

<?php
  include ("includes/footer.php");
?>