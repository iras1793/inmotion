
            <div class="portada" id="headerbmwmini"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">BMW - Mini</h1>
                  <h4 class="subtitulodos txtgray">Pop-up store</h4>
                  <p class="txt2 txtgray">Diseño, producción e implementación de pop-up
store para diferentes eventos como son: eventos
deportivos, entretenimiento, workshops, entre otros.
                  </p>
                  <p class="txt2 txtgray">A partir de las necesidades de nuestro cliente,
desarrollamos un sistema modular de pop-up store
que nos permite montar en diferentes formatos,
dependiendo de los requerimientos de cada evento.</p>
                   
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-2.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-3.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-4.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-5.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_mini/BMW_POPUP-6.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
