<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
	<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	
	<title><?php echo $title; ?></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/logotipo.svg') ?>" />
	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="<?php echo base_url('assets/img/logotipo.svg') ?>">
	<!-- Place favicon.ico in the root directory -->

	<link rel="stylesheet" href="<?php echo base_url('assets/css/normalize.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css') ?>">

	<meta name="theme-color" content="#fafafa">
	</head>
 
	<body id="page">

		<header id="header">
		 <div class="contenedor">
		      <div id="logotipo"><a href="<?php echo site_url("inicio") ?>"><img src="<?php echo base_url("assets/img/logotipo.svg") ?>"></a></div>
		      
		      <p class="seccionactual"><a href="<?php echo $redirect ?>"><?php echo $pagina ?></a></p>                   
		      <div class="menu-collapsed">
		         <div class="bar"></div>
		         <nav>
		            <ul class="menu">
		               <li><a href="<?php echo site_url("inicio/index#header")  ?>" class="cerrarMenu">Home</a></li>
		               <li><a href="<?php echo site_url("inicio/services")  ?>" class="cerrarMenu">Services</a></li>
		               <li><a href="<?php echo site_url("inicio/index#clients")  ?>" class="cerrarMenu">Clients</a></li>
		               <li><a href="<?php echo site_url("inicio/index#work")  ?>" class="cerrarMenu">Work</a></li>
		               <li><a href="<?php echo site_url("inicio/index#keyplayers")  ?>" class="cerrarMenu">Key Players</a></li>
		               <li><a href="<?php echo site_url("inicio/index#contacto")  ?>" class="cerrarMenu">Get in touch</a></li>
		               <li><a href="#modalcode" id="downloads" class="cerrarMenu">Downloads</a></li>
		            </ul>
		              <ul class="datoscontacto">
		              <li><a href="mailto:info@inmotion.com.mx">info@inmotion.com.mx</a></li>
		              <li><a href="tel:5553938627" class="telefono">Tel. 55 5393 8627</a></li>
		              <li><p class="direccion">
		                 Monte Elbruz 132, Int. 101,
		                 <br>11850, Col. San Miguel Chapultepec,
		                 <br>CDMX
		                </p>
		              </li>
		              <div class="social">
		                  <div class="redes">
		                      <a href="https://www.facebook.com/inmotionmktg/" target="_blank" class="redes" id="facebook"><img src="<?php echo base_url("assets/img/facebook.svg") ?>"></a>
		                      <a href="https://www.instagram.com/inmotionmktg/" target="_blank" class="redes" id="instagram"><img src="<?php echo base_url("assets/img/instagram.svg") ?>"></a>
		                      <a href="https://www.linkedin.com/company/inmotion-communications" target="_blank" class="redes" id="linkedin"><img src="<?php echo base_url("assets/img/linkedin.svg") ?>"></a>
		                      <a href="https://vimeo.com/inmotionmktgMailchimp" target="_blank" class="redes" id="vimeo"><img src="<?php echo base_url("assets/img/vimeo.svg") ?>"></a>
		                  </div>
		              </div>          
		          </ul>
		         </nav>                  
		      </div>
		      <div class="overlay"></div>
		  </div>
		</header>
		<main>
			

			<?php echo $content; ?>

		</main>

		<footer>
			<div class="block">
				<div class="creditos">
			    	<p class="copy">&copy2019. <span>Todos los derechos reservados<br>CUADRO NARANJA COMUNICACION</span></p>
			    	
			    	<p class="develop"><a href="http://www.irasdargor.com.mx">Development by: <img src="<?php echo base_url('assets/img/imagotipo_iras-morado.svg') ?>"></a></p>
				</div>
			</div>
		</footer>
              

		<!-- Modal -->
		<div id="modalbox">
		  <div class="modal__cont">
		    <a href="#modalCloseBtn" class="modalCloseBtn">&times;</a>
		    <h2 class="parallax__title txtorange">Downloads</h2>
		    <div class="containerD">
		      <div class="items">
		        <a href="<?php echo base_url("assets//pdf/cv_inmotiongroup2019.pdf") ?>" download="CV INMOTION GROUP 2019">
		        <div class="image-wrapper">
		          <img src="<?php echo base_url("assets/img/nmgroup.svg") ?>">
		        </div>
		        <div class="project-name">
		          <p>Inmotion group CV</p>
		        </div>
		        </a>
		      </div>
		      <div class="items">
		         <a href="<?php echo base_url("assets//pdf/nm_credenciales2019.pdf") ?>" download="NM CREDENCIALES 2019">
		        <div class="image-wrapper">
		          <img src="<?php echo base_url("assets/img/nmgroupgray.svg") ?>">
		        </div>
		        <div class="project-name">
		          <p>NM Credenciales</p>
		        </div>
		        </a>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!-- Modal en construcci贸n -->
		<div id="modalconstruccion">
		  <div class="modal__cont">
		    <a href="#modalCloseBtn" class="modalCloseBtn">&times;</a>
		    <h2 class="parallax__title">Under <span class="txtorange"> construction</span></h2>
		    <div class="containerD">
		        <p>Sorry for the inconvenience, we are renewing ourselves to give you a better service.</p>
		    </div>
		    </div>
		  </div>
		</div>
		
		<!-- Modal codigo downloads -->
		<div id="modalcode">
		  <div class="modal__cont">
		    <a href="#modalCloseBtn" class="modalCloseBtn">&times;</a>
		    <h2 class="parallax__title txtorange">Downloads</h2>
		    <div class="containerD">
		      	<form class="form" id="access"> 
		      	    <p>Ingresa tu codigo</p>
		      		<input  type="text" name="code" placeholder="46HKL5">
		      		<button class="submit entry" onclick="thanks()">Send</button>
		      	</form>
		    </div>
		  </div>
		</div>
		
		<!-- Aviso Cookies -->
		   <div class="cookie-jar" data-myval="false">
        	    <div class="cookie-jar-content">
        		    <?php echo $cookies; ?>
        		    <a href="javascript:void(0);" title="" id="btnAccept" class="buttoncookie">Aceptar</a>
        	    </div>
        	</div>

    	<!-- Modal aviso privacidad -->
		<div id="modalprivacidad">
		  <div class="modal__cont">
		  	<div class="containerModal">
			    <a href="#modalCloseBtn" class="modalCloseBtn">&times;</a>
			    <h2 class="parallax__title txtorange">Pol&iacute;ticas de Privacidad</h2>
			    <div class="containerD">
			    	<!-- <p>La presente Política de Privacidad establece los términos en que CUADRO NARANJA COMUNICACION usa y protege la información que es proporcionada por sus usuarios al momento de utilizar su sitio web. Esta compañía está comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de información personal con la cual usted pueda ser identificado, lo hacemos asegurando que sólo se empleará de acuerdo con los términos de este documento. Sin embargo esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos y enfatizamos revisar continuamente esta página para asegurarse que está de acuerdo con dichos cambios.
			    	</p>
			    	<p>
			    		<span class="boldtitle">Información que es recogida</span>
			    		<br><br>
						Nuestro sitio web podrá recoger información personal por ejemplo: Nombre, información de contacto como su dirección de correo electrónica e información demográfica. Así mismo cuando sea necesario podrá ser requerida información específica para procesar algún pedido o realizar una entrega o facturación.
					</p>

					<p>
			    		<span class="boldtitle">Uso de la información recogida</span>
			    		<br><br>
						Nuestro sitio web emplea la información con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros productos y servicios. Es posible que sean enviados correos electrónicos periódicamente a través de nuestro sitio con ofertas especiales, nuevos productos y otra información publicitaria que consideremos relevante para usted o que pueda brindarle algún beneficio, estos correos electrónicos serán enviados a la dirección que usted proporcione y podrán ser cancelados en cualquier momento.
						<br><br>
						CUADRO NARANJA COMUNICACION está altamente comprometido para cumplir con el compromiso de mantener su información segura. Usamos los sistemas más avanzados y los actualizamos
						constantemente para asegurarnos que no exista ningún acceso no autorizado.
					</p>

					<p>
			    		<span class="boldtitle">Cookies</span>
			    		<br><br>
						Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener información respecto al tráfico web, y también facilita las futuras visitas a una web recurrente. Otra función que tienen las cookies es que con ellas las web pueden reconocerte individualmente y por tanto brindarte el mejor servicio personalizado de su web.
						<br><br>
						Nuestro sitio web emplea las cookies para poder identificar las páginas que son visitadas y su frecuencia. Esta información es empleada únicamente para análisis estadístico y después la información se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un mejor servicio de los sitios web, estás no dan acceso a información de su ordenador ni de usted, a menos de que usted así lo quiera y la proporcione directamente, visitas a una web . Usted puede aceptar o negar el uso de cookies, sin embargo la mayoría de navegadores aceptan cookies automáticamente pues sirve para tener un mejor servicio web. También usted puede cambiar la configuración de su ordenador para declinar las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.
					</p>

					<p>
			    		<span class="boldtitle">Enlaces a Terceros</span>
			    		<br><br>
						Este sitio web pudiera contener en laces a otros sitios que pudieran ser de su interés. Una vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con estas.
					</p>

					<p>
			    		<span class="boldtitle">Control de su información personal</span>
			    		<br><br>
						En cualquier momento usted puede restringir la recopilación o el uso de la información personal que es proporcionada a nuestro sitio web. Cada vez que se le solicite rellenar un formulario, como el de alta de usuario, puede marcar o desmarcar la opción de recibir información por correo electrónico. En caso de que haya marcado la opción de recibir nuestro boletín o publicidad usted puede cancelarla en cualquier momento.
						<br><br>
						Esta compañía no venderá, cederá ni distribuirá la información personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.
						<br>
						CUADRO NARANJA COMUNICACION Se reserva el derecho de cambiar los términos de la presente Política de Privacidad en cualquier momento.
					</p> -->
					<?php echo $politicas; ?>
				</div>		      	
		    </div>
		  </div>
		</div>

		<script src="<?php echo base_url('assets/js/vendor/modernizr-3.7.1.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
		<script>window.jQuery || document.write('<script src="<?php echo base_url('assets/js/vendor/jquery-3.4.1.min.js') ?>"><\/script>')</script>
		<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.touchSwipe.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/plugins.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
		<script>
			$(document).ready(function() {
				var hideCookies = <?php echo $hideCookies; ?>;
				console.log("hideCookies: "+hideCookies);
		      
				$("#btnAccept").click(function(){
					$(".cookie-jar").hide();
				});
		      
				if (hideCookies==true) {
					$(".cookie-jar").hide();
				}
			});
		</script>
	</body>

</html>
