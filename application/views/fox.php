
            <div class="portada" id="headerfoxoficinas"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">FOX Networks Group</h1>
                  <h4 class="subtitulodos txtgray">Oficinas FOX</h4>
                  <p class="txt2 txtgray">En 2018 FOX Networks Group cambió de ubicación y
aceptamos la misión de acondicionar el nuevo
espacio con su imagen de marca.
                  </p>
                  <p class="txt2 txtgray">Diseñamos todos los espacios trayendo el mundo
FOX a la vida real.</p>
                   
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-2.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-3.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-4.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-5.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-6.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-7.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-8.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-9.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-10.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-11.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-12.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-13.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-14.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-15.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/fox_oficinas/FOX_OFICINAS-16.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

       <div class="enlaceback"><a href="<?php echo site_url("inicio/branding")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Branding </span></p></a>   <hr> </div>