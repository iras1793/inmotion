<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="x-ua-compatible" content="IE=10">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $title; ?></title>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/admin/images/logotipo.svg') ?>" />
  <meta name="description" content="A premium collection of beautiful hand-crafted Bootstrap 4 admin dashboard templates and dozens of custom components built for data-driven applications.">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/font-awesome.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/fonts.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/bootstrap.css') ?>" >
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/shards-dashboards.1.3.1.min.css') ?>" id="main-stylesheet" data-version="1.3.1">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/extras.1.3.1.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/preloader.css') ?>">
  <script async defer src="<?php echo base_url('assets/admin/js/git-buttons.js') ?>"></script>
  <!-- custom js -->
  <?php if (isset($estilos)): ?>  
    <?php foreach ($estilos as $key => $css): ?>
        <link rel="stylesheet" href="<?php echo base_url($css) ?>" >
    <?php endforeach ?>
  <?php endif ?>
  <!-- end custom js -->

</head>


<body class="h-100">

<!-- preloader -->
<div class="loader" id="preloader">
  <div class="loader-inner">
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
    <div class="loader-line-wrap">
      <div class="loader-line"></div>
    </div>
  </div>
</div>
<!-- end preloader -->

  <div class="container-fluid">
    <div class="row">
      <main class="main-content col-lg-12 col-md-12 col-sm-12 p-0">
        <div class="main-navbar  bg-white">
          <div class="container p-0">
            <!-- Main Navbar -->
            <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">
              
              <a class="navbar-brand" href="#" style="line-height: 25px;">
                <div class="d-table m-auto">
                  <img id="main-logo" class="d-inline-block align-top mr-1 ml-3" style="max-width: 45px;" src="<?php echo base_url('assets/admin/images/logotipo.png') ?>" alt="Inmotion-logo">
                  
                </div>
              </a>


              <ul class="navbar-nav border-left flex-row border-right ml-auto">

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="user-avatar rounded-circle mr-2" src='<?php echo base_url("assets/admin/images/nmgroup.svg") ?>' alt="User Avatar"> 
                    <span class="d-none d-md-inline-block"><?php echo $this->auth->userName(); ?></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-small">
                    <?php $iduser = $this->auth->userid(); ?>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="<?php echo site_url('login/logout') ?>">
                      <i class="material-icons text-danger">&#xE879;</i> Salir </a>
                    </div>
                </li>
              </ul>

              <nav class="nav">
                <a href="#" class="nav-link nav-link-icon toggle-sidebar  d-inline d-lg-none text-center " data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
                  <i class="material-icons">&#xE5D2;</i>
                </a>
               </nav>
            </nav>
          </div> <!-- / .container -->
        </div> <!-- / .main-navbar -->
        
        <div class="header-navbar collapse d-lg-flex p-0 bg-white border-top">
          <div class="container">
            <div class="row">
              <div class="col">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                  <li class="nav-item">
                    <a href="<?php echo site_url('dashboard') ?>" class="<?php echo ( $pagina=='dashboard' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                  </li>
                   <li class="nav-item">
                    <a href="<?php echo site_url('dashboard/politicas') ?>" class="<?php echo ( $pagina=='politicas' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-lock "></i> Política de privacidad</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('dashboard/cookies') ?>" class="<?php echo ( $pagina=='cookies' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-cog"></i> Cookies</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('dashboard/keyplayers') ?>" class="<?php echo ( $pagina=='keyplayers' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-user"></i> Keyplayers</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('dashboard/clientes') ?>" class="<?php echo ( $pagina=='clientes' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-education"></i> Clientes</a>
                  </li>
                  <!--
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/socios') ?>" class="<?php echo ( $miga_pan=='Socios' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-copy"></i> Socios</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/paquete') ?>" class="<?php echo ( $miga_pan=='Paquete' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-folder-open"></i> Paquetes</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/clases') ?>" class="<?php echo ( $miga_pan=='Clases' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-blackboard"></i> Clases</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/productos') ?>" class="<?php echo ( $miga_pan=='Productos' )  ? 'nav-link active' : 'nav-link'; ?>" ><i class="glyphicon glyphicon-shopping-cart"></i> Productos</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/entradas') ?>" class="<?php echo ( $miga_pan=='Entradas' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-open"></i> Entradas</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/salidas') ?>" class="<?php echo ( $miga_pan=='Salidas' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-usd"></i> Ventas</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/registro') ?>" class="<?php echo ( $miga_pan=='Registro' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-calendar"></i> Registro</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/reportes') ?>" class="<?php echo ( $miga_pan=='Reportes.' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-stats"></i> Reportes</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo site_url('principal/cortecaja') ?>" class="<?php echo ( $miga_pan=='Corte de caja' )  ? 'nav-link active' : 'nav-link'; ?>"><i class="glyphicon glyphicon-usd"></i> Corte de caja</a>
                  </li> -->
                </ul>
              </div>
            </div>
          </div>
        </div>


      <div class="main-content-container container">

         <!-- Page Header -->
         <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
               <span class="text-uppercase page-subtitle"><?php echo $encabezado ?></span>
               <h3 class="page-title"><?php echo $miga_pan ?></h3>
            </div>
         </div>


         <div class="row mb-2">
<!--
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/usuarios') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Usuarios' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/man-user.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Usuarios
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/roles') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Roles' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/locked-padlock.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Roles
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/socios') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Socios' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/multiple-users-silhouette.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Socios
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/paquete') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Paquete' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/folder-plus-solid.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Paquete
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/clases') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Clases' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/bike.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Clases
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/productos') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Productos' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/shopping-cart.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Productos
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/entradas') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Entradas' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/cart-up.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Entradas
              </div>
            </a>
          </div>
-->          
<!--           <div class="col mb-4">
            <a href="<?php echo site_url('principal/salidas') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Salidas' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/money.svg') ?>" alt="" class="img-fluid" style="max-width: 85px;"> 
                <br>Ventas
              </div>
            </a>
          </div> -->
<!--
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/registro') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Registro' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/calendar.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Registro
              </div>
            </a>
          </div>
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/reportes') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Reportes.' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/analysis.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Reportes
              </div>
            </a>
          </div>
          
          <div class="col mb-4">
            <a href="<?php echo site_url('principal/cortecaja') ?>" style="text-decoration: none;">
              <div class="<?php echo ( $miga_pan=='Corte de caja' ) ? 'bg-primary rounded text-white text-center p-2' : 'bg-secondary rounded text-white text-center p-2' ?>" style="margin:10px;box-shadow: inset 0 0 5px rgba(0,0,0,.2);">
                <img src="<?php echo base_url('assets/admin/images/icon/cash-register-solid.svg') ?>" alt="" class="img-fluid" style="max-width: 25px;"> 
                <br>Corte Caja
              </div>
            </a>
          </div>
-->
        </div>
 

        <?php echo $content; ?>

      </div> <!-- termina main-content-container container -->



      </main>

    </div>

  </div>

  <!-- generic js -->
  <script src="<?php echo base_url('assets/admin/js/loader.js') ?>" type="text/javascript"></script>
  <script src="<?php echo base_url('assets/admin/js/jquery.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin/js/popper.js') ?>"></script>
  <script src="<?php echo base_url('assets/admin/js/bootstrap.js') ?>"></script>
  <!-- end generic js -->
  
  <!-- custom js -->
  <?php foreach ($javascript as $key => $js): ?>
      <script type="text/javascript" src="<?php echo base_url($js) ?>"></script>
  <?php endforeach ?>
  <script>
    $(window).on('load', function() {
        $('#preloader').remove();
        //$('body').removeClass('preloader');
    });
  </script>
  
  <script>
    
    $(window).on('load', function() {
        if (window.jQuery) {  
          $(document).ready(function() {
            var titulo = $('#tableTitle').text();
            var table =  $('#genericTable').DataTable( {
                  dom: 'Bfrtip',
                  scrollX: true,
                  buttons: [
                      {
                        extend: 'excel',
                        title:titulo,
                        footer: true,
                        exportOptions: {
                          columns: 'thead th:not(#noExport)'
                        }
                      },
                      {
                        extend: 'pdf',
                        title:titulo,
                        footer: true,
                        exportOptions: {
                          columns: 'thead th:not(#noExport)'
                        }
                      }
                  ]
            });

            table.button( 0 ).nodes().css( 'background', '#3ba858' );
            table.button( 0 ).nodes().css( 'color', '#FFFFFF' );
            table.button( 1 ).nodes().css( 'background', '#ff4554' );
            table.button( 1 ).nodes().css( 'color', '#FFFFFF' );
            /*table.button( 0 ).text('');*/




          });
        }
    });

  </script>    

  <!-- end custom js -->


</body>


</html>