
  <?php echo _print_messages(); ?>
    
  <div class="row">
    <div class="col">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle">Registros</h6>
        </div>
        <div class="">
            <a href="<?php echo site_url('dashboard/keyplayers') ?>" class="btn btn-success float-right ml-1 mr-1 mt-1 mb-2"><i class="glyphicon glyphicon-chevron-left"></i> Regresar a Keyplayers</a>
<?php echo form_open("dashboard/ordenar_keyplayers"); ?>  
          <table class="" id="genericTable">
            
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Nombre</th>
                <th scope="col" class="border-0">Cargo</th>
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
            <?php if (is_array($registros)): ?>
              <?php $i=1; ?>
              <?php foreach ($registros as $key => $datos): ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $datos->nombre ?></td>
                    <td><?php echo $datos->cargo ?></td>

                    <td class="bg-white">
                      <select name="<?php echo "orden_".$datos->id ?>" class="custom-select">
                          <?php for($j=1; $j<=$totales; $j++): ?>
                            <?php if($datos->orden==$j): ?>
                            <option value="<?php echo $j ?>" selected ><?php echo $j ?></option>
                            <? else: ?>
                            <option value="<?php echo $j ?>" ><?php echo $j ?></option>
                            <?php endif; ?>
                          <?php endfor; ?>
                      </select>
                    </td>
                    
                  </tr>
                  <?php $i++; ?>
              <?php endforeach ?>
            <?php endif ?>
            </tbody>

            
          </table>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Guardar">
            </div>
            </form>
        </div>
      </div>
    </div>
  </div>

<script>
  
  window.onload = function(){
      if (window.jQuery) {

          $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-success").slideUp(500);
          });

          $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-danger").slideUp(500);
          });

      }
  };


</script>