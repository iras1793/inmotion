<style>
  .error{height: 30px;}
  #nombre-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #cargo-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #linkedin-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #imagen_keyplayer-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
        <?php echo _print_messages(); ?>
          <a href="<?php echo site_url('dashboard/keyplayers') ?>" class="btn btn-info mb-3">Regresar</a>
          <!-- Edit User Details Card -->
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">Datos generales</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("dashboard/$action/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>

                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="nombre">Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="nombre" value="<?php echo $fields['nombre']['value'] ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="cargo">Cargo / Puesto</label>
                        <input type="text" name="cargo" class="form-control" id="cargo" value="<?php echo $fields['cargo']['value'] ?>">
                      </div>

                      <div class="form-group col-md-12">
                        <label for="linkedin">Linkedin: URL</label>
                        <input type="text" name="linkedin" class="form-control" id="linkedin" value="<?php echo $fields['linkedin']['value'] ?>">
                      </div>

                      <div class="col-lg-12">
                        <label for="imagen_keyplayer" class="text-center w-100 mb-4">Imagen</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen = ($fields['imagen_keyplayer']['value']!='') ? $fields['imagen_keyplayer']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/keyplayers/$imagen") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_keyplayer" accept="image/*" id="imagen_keyplayer" class="d-none">
                      </div>

                    </div>
                  </div>

                </div>

              
            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Guardar">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
 window.onload = function(){
    
    
    if (window.jQuery) {

        $("#form-validate").validate({
            rules: {
                nombre: {
                    required : true,
                    minlength: 2,
                    maxlength: 100
                },
                cargo: {
                    required : true,
                    minlength: 3,
                    maxlength: 100
                },
                linkedin: {
                    required : true,
                    url: true
                },
                imagen_keyplayer: {
                    accept: "image/*"
                }
            },
            messages:{
                nombre:{
                    required: "El nombre es obligatorio",
                    minlength: "Ingresa una cadena de al menos 3 caracteres",
                    maxlength: "No aceptamos más de 100 caracteres"
                },
                cargo:{
                    required: "El cargo es obligatorio",
                    minlength: "Ingresa una cadena de al menos 3 caracteres",
                    maxlength: "No aceptamos más de 100 caracteres"
                },
                linkedin:{
                    required: "La url de linkedin  es obligatorio",
                    url: "Ingresa una url válida"
                },
                imagen_keyplayer:{
                    accept: "Sólo aceptamos archivos de tipo imagen"
                }
            }
        });

        $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-success").slideUp(500);
        });

        $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-danger").slideUp(500);
        });
        
    }//end if
 }//end window onload

</script>