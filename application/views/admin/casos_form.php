<style>
  .error{height: 30px;}
  #firstName-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #lastName-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #teaser-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #fondo-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #seccion-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #rol-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #repetir-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #our_work-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
        <?php echo _print_messages(); ?>
          <a href="<?php echo site_url('dashboard') ?>" class="btn btn-info mb-3">Regresar</a>
          <!-- Edit User Details Card -->
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">Datos generales</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("dashboard/$action/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>

                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="firstName">Título</label>
                        <input type="text" name="titulo" class="form-control" id="firstName" value="<?php echo $fields['titulo']['value'] ?>">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="lastName">Subtítulo</label>
                        <input type="text" name="subtitulo" class="form-control" id="lastName" value="<?php echo $fields['subtitulo']['value'] ?>">
                      </div>

                      <div class="col-lg-6">
                        <label for="teaser" class="text-center w-100 mb-4">Imagen Teaser</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen_teaser = ($fields['imagen_teaser']['value']!='') ? $fields['imagen_teaser']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/nuevas/$imagen_teaser") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_teaser" accept="image/*" id="teaser" class="d-none">
                      </div>
                      <div class="col-lg-6">
                        <label for="fondo" class="text-center w-100 mb-4">Imagen Fondo</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen_fondo = ($fields['imagen_fondo']['value']!='') ? $fields['imagen_fondo']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/nuevas/$imagen_fondo") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_fondo" accept="image/*" id="fondo" class="d-none">
                      </div>

                      <div class="form-group col-md-6" style="margin-top: 25px;">
                        <label for="displayEmail">Sección</label>
                        <select class="custom-select" name="seccion">
                          <option value="">Seleccionar sección</option>
                          <option value="Events" <?php echo ($fields['seccion']['value']=='Events') ? 'selected':'' ?>>Events</option>
                          <option value="ExperimentalMarketing" <?php echo ($fields['seccion']['value']=='ExperimentalMarketing') ? 'selected':'' ?>>Experimental Marketing</option>
                          <option value="Custom" <?php echo ($fields['seccion']['value']=='Custom') ? 'selected':'' ?>>Custom, stands and display</option>
                          <option value="Branding" <?php echo ($fields['seccion']['value']=='Branding') ? 'selected':'' ?>>Branding</option>
                        </select>
                      </div>
                    
                    <div class="form-group col-md-6" style="margin-top: 25px;">
                        <label for="displayEmail">¿Aparecerá en sección "Our Work"?</label>
                        <select class="custom-select" name="our_work">
                          <option value="">Selecciona opción</option>
                          <option value="1" <?php echo ($fields['our_work']['value']=='1') ? 'selected':'' ?>>Sí</option>
                          <option value="0" <?php echo ($fields['our_work']['value']=='0') ? 'selected':'' ?>>No</option>
                        </select>
                    </div>


                    <div class="form-group col-md-12" style="margin-top: 25px;">
                      <!-- https://vimeo.com/inmotionmktg -->
                      <label for="video">URL del video</label>
                      <input type="text" name="video" class="form-control" id="video" value="<?php echo $fields['video']['value'] ?>">
                    </div>


                      <div class="form-group col-md-12">
                        <br>
                        <label for="userLocation">Descripción del contenido</label>
                          <main>
                              <div id="editor">
                                <?php echo $fields['descripcion']['value'] ?>
                              </div>
                          </main>
                      </div>
                      <div class="form-group col-md-12">
                          <input type="text" name="descripcion" class="form-control" id="desc" readonly>
                      </div>
                      <hr>
                      <label for="userProfilePicture" class="text-center w-100 mb-4">Imágenes secuenciales</label><br><br>
                      
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mb-1">1</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen_fondo = ($fields['imagen_1']['value']!='') ? $fields['imagen_1']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/nuevas/$imagen_fondo") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_1" accept="image/*" id="userProfilePicture" class="d-none">
                        
                        <div class="custom-control custom-checkbox mt-3 ml-2">
                          <input type="checkbox" class="custom-control-input" id="formsCheckboxDefault" name="eliminar_1">
                          <label class="custom-control-label" for="formsCheckboxDefault">Eliminar imagen</label>
                        </div>

                      </div>

                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mb-1">2</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen_fondo = ($fields['imagen_2']['value']!='') ? $fields['imagen_2']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/nuevas/$imagen_fondo") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_2" accept="image/*" id="userProfilePicture" class="d-none">

                        <div class="custom-control custom-checkbox mt-3 ml-2">
                          <input type="checkbox" class="custom-control-input" id="imagen_2" name="eliminar_2">
                          <label class="custom-control-label" for="imagen_2">Eliminar imagen</label>
                        </div>

                      </div>

                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mb-1">3</label>
                        <div class="edit-user-details__avatar m-auto">
                          <?php $imagen_fondo = ($fields['imagen_3']['value']!='') ? $fields['imagen_3']['value'] : 'logotipo.svg'; ?>
                          <img src='<?php echo base_url("assets/gallery/nuevas/$imagen_fondo") ?>' alt="Avatar">
                          <label class="edit-user-details__avatar__change">
                          </label>
                        </div>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_3" accept="image/*" id="userProfilePicture" class="d-none">

                        <div class="custom-control custom-checkbox mt-3 ml-2">
                          <input type="checkbox" class="custom-control-input" id="imagen_3" name="eliminar_3">
                          <label class="custom-control-label" for="imagen_3">Eliminar imagen</label>
                        </div>

                      </div>

                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">4</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_4" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>

                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">5</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_5" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>

                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">6</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_6" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">7</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_7" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">8</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_8" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">9</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_9" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">10</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_10" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">11</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_11" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">12</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_12" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">13</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_13" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">14</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_14" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">15</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_15" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">16</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_16" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">17</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_17" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">18</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_18" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">19</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_19" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>
                      <div class="col-lg-4 mb-5">
                        <label for="userProfilePicture" class="text-center w-100 mt-3">20</label>
                        <input type="file" class="btn btn-sm btn-white d-table mx-auto mt-4" name="imagen_20" accept="image/*" id="userProfilePicture" class="d-none">
                      </div>


                    </div>
                  </div>

                </div>

              
            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Guardar">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
 window.onload = function(){
    initSample();
    
    if (window.jQuery) {
        jQuery.validator.addMethod("getDesc", function(value, element) {
          var des = CKEDITOR.instances.editor.getData();
          if(des!=''){
            $("#desc").val(des);
            return true;
          }
          return false;
        }, "La descripción es obligatoria");

        $("#form-validate").validate({
            rules: {
                titulo: {
                    required : true,
                    minlength: 2,
                    maxlength: 100
                },
                subtitulo: {
                    required : true,
                    minlength: 3,
                    maxlength: 100
                },
                descripcion: {
                    getDesc: true
                },
                seccion: {
                    required : true
                },
                our_work: {
                    required : true
                },
                imagen_teaser: {
                    required : false,
                    accept: "image/*"
                },
                imagen_fondo: {
                    required : false,
                    accept: "image/*"
                }
            },
            messages:{
                titulo:{
                    required: "El titulo es obligatorio",
                    minlength: "Ingresa una cadena de al menos 3 caracteres",
                    maxlength: "No aceptamos más de 100 caracteres"
                },
                subtitulo:{
                    required: "El subtitulo es obligatorio",
                    minlength: "Ingresa una cadena de al menos 3 caracteres",
                    maxlength: "No aceptamos más de 100 caracteres"
                },
                seccion:{
                    required: "La sección es obligatoria"
                },
                our_work:{
                    required: "Elige una opción"
                },
                imagen_teaser:{
                    required: "Se recomienda agregar una imagen",
                    accept: "Sólo aceptamos archivos de tipo imagen"
                },
                imagen_fondo:{
                    required: "Se recomienda agregar una imagen",
                    accept: "Sólo aceptamos archivos de tipo imagen"
                }
            }
        });

        $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-success").slideUp(500);
        });

        $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-danger").slideUp(500);
        });
        
    }//end if
 }//end window onload

</script>