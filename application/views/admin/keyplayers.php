
<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">¿Seguro que deseas eliminar el registro?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Estas por eliminar un registro
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a>
        <a href="#" class="btn btn-success" id="delete">Sí, eliminar</a>
      </div>
    </div>
  </div>
</div>


  <div class="row">
    <div class="col">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle">Registros</h6>
        </div>
        <div class="">
          <a href="<?php echo site_url('dashboard/agregar_keyplayers') ?>" class="btn btn-success float-right ml-1 mr-1 mt-1 mb-2"><i class="fas fa-plus"></i> Agregar nuevo Keyplayer</a>
          <a href="<?php echo site_url('dashboard/ordenar_keyplayers') ?>" class="btn btn-info float-right ml-1 mr-1 mt-1 mb-2"><i class="glyphicon glyphicon-list"></i> Ordenar Keyplayers</a>
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Nombre</th>
                <th scope="col" class="border-0">Cargo / Puesto</th>
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
            <?php if (is_array($keyplayers)): ?>
              <?php $i=1; ?>
              <?php foreach ($keyplayers as $key => $datos): ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $datos->nombre ?></td>
                    <td><?php echo $datos->cargo ?></td>
                    <td class="bg-white">
                      <a href='<?php echo site_url("dashboard/editar_keyplayers/$datos->id") ?>' class="btn btn-warning text-white mt-1 mb-1">Editar</a>
                      <a href="javascript:void(0);" class="btn btn-danger mt-1 mb-1 eliminar" id="<?php echo $datos->id ?>">Eliminar</a>
                    </td>
                  </tr>
                  <?php $i++; ?>
              <?php endforeach ?>
            <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<script>
  
  window.onload = function(){
      if (window.jQuery) {

          $("#delete-modal").hide();
          
          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url('dashboard/eliminar_keyplayers/') ?>"+event.target.id);
              });
          });

          $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-success").slideUp(500);
          });

          $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-danger").slideUp(500);
          });


      }
  };


</script>