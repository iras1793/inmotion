
<!-- <div class="alert alert-success" id="success-alert">
  <strong id="success-text"></strong>
</div>
<div class="alert alert-danger" id="danger-alert">
  <strong id="danger-text"></strong>
</div> -->

<?php echo _print_messages(); ?>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">¿Seguro que deseas eliminar el registro?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Estas por eliminar un registro
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a>
        <a href="#" class="btn btn-success" id="delete">Sí, eliminar</a>
      </div>
    </div>
  </div>
</div>


  <div class="row">
    <div class="col">
      <div class="card card-small mb-4">
        <div class="card-header border-bottom">
          <h6 class="m-0" id="tableTitle">Registros</h6>
        </div>
        <div class="">
          <a href="<?php echo site_url('dashboard/agregar') ?>" class="btn btn-success float-right ml-1 mr-1 mt-1 mb-2"><i class="fas fa-plus"></i> Agregar nuevo caso de éxito</a>
          
          <table class="" id="genericTable">
            <thead class="bg-light">
              <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Título</th>
                <th scope="col" class="border-0">Subtitulo</th>
                <th scope="col" class="border-0">Sección</th>
                <th scope="col" class="border-0">Aparece en Home</th>
                <th scope="col" class="border-0" id="noExport"></th>
              </tr>
            </thead>
            <tbody>
            <?php if (is_array($casos)): ?>
              <?php $i=1; ?>
              <?php foreach ($casos as $key => $datos): ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $datos->titulo ?></td>
                    <td><?php echo $datos->subtitulo ?></td>
                    <td><?php echo $datos->seccion ?></td>
                    <td><?php echo ($datos->our_work=='1') ? 'Sí':'No' ?></td>
                    <td class="bg-white">
                      <a href='<?php echo site_url("dashboard/editar/$datos->id") ?>' class="btn btn-warning text-white mt-1 mb-1">Editar</a>
                      <a href="javascript:void(0);" class="btn btn-danger mt-1 mb-1 eliminar" id="<?php echo $datos->id ?>">Eliminar</a>
                    </td>
                  </tr>
                  <?php $i++; ?>
              <?php endforeach ?>
            <?php endif ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<script>
  
  window.onload = function(){
      if (window.jQuery) {
/*          $("#success-alert").hide();
          $("#danger-alert").hide();*/


          $("#delete-modal").hide();
          
          $('.eliminar').click(function eliminar(event){
              $('.modal-body').load('/render/62805',function(result){
                $('#exampleModalCenter').modal({show:true});
                $('#delete').attr("href", "<?php echo site_url('dashboard/eliminar/') ?>"+event.target.id);
              });
          });

          $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-success").slideUp(500);
          });

          $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
            $(".alert-danger").slideUp(500);
          });


      }
  };


</script>