<style>
  .error{height: 30px;}
  #firstName-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #lastName-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #teaser-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #fondo-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #seccion-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #rol-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
  #repetir-error{background-color:#ffa29c;margin-top: 5px;color: #ff0f00;border-radius: 5px;padding: 5px 15px 5px 15px;}
</style>

    <div class="row">
        <div class="col-lg-10 mx-auto mt-4">
        <?php echo _print_messages(); ?>
          
          <!-- Edit User Details Card -->
          <div class="card card-small edit-user-details mb-4">
            <div class="card-body p-0">
              <div class="border-bottom clearfix d-flex">
                <ul class="nav nav-tabs border-0 mt-auto mx-4 pt-2">
                  <li class="nav-item">
                    <a class="nav-link active" href="#">Datos generales</a>
                  </li>
                </ul>
              </div>
              <?php echo form_open_multipart("dashboard/cookies/$id", array('class'=>'py-4', 'id'=>'form-validate')); ?>

                <div class="form-row mx-4">
                  <div class="col-lg-12">
                    <div class="form-row">
                    
                      <div class="form-group col-md-12">
                        <br>
                        <label for="userLocation">Descripción de las cookies</label>
                          <main>
                              <div id="editor">
                                <?php echo $fields['cookies']['value'] ?>
                              </div>
                          </main>
                      </div>
                      <div class="form-group col-md-12">
                          <input type="text" name="cookies" class="form-control" id="desc" readonly>
                      </div>
                      

                    </div>
                  </div>

                </div>

              
            </div>
            <div class="card-footer border-top">
              <input type="submit" class="btn btn-accent ml-auto d-table mr-3" value="Guardar">
            </div>
            </form>
          </div>
          <!-- End Edit User Details Card -->
        </div>
    </div> 

<script>
  
 window.onload = function(){
    initSample();
    
    if (window.jQuery) {
        jQuery.validator.addMethod("getDesc", function(value, element) {
          var cookies = CKEDITOR.instances.editor.getData();
          if(cookies!=''){
            $("#desc").val(cookies);
            return true;
          }
          return false;
        }, "La descripción es obligatoria");

        $("#form-validate").validate({
            rules: {
                cookies: {
                    getDesc: true
                }
            }
        });

        $(".alert-success").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-success").slideUp(500);
        });

        $(".alert-danger").fadeTo(2000, 500).slideUp(500, function() {
          $(".alert-danger").slideUp(500);
        });
        
    }//end if
 }//end window onload

</script>