<!--
             
            <div class="banner" id="natgeokids">
               <a href="<?php #echo site_url("inicio/natgeokids")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">NATGEO Kids</h1>
                  <h4 class="subtitulo">Natgeo Lab, raro pero cierto</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
            <div class="banner" id="kiaformulam">
               <a href="<?php #echo site_url("inicio/kiaformulam")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">KIA </h1>
                  <h4 class="subtitulo"> Fórmula M</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
            <div class="banner" id="longines">
               <a href="<?php #echo site_url("inicio/longines")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">FOX Sports</h1>
                  <h4 class="subtitulo">Longines Global Champions Tour</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
            <div class="banner" id="mobil">
               <a href="<?php #echo site_url("inicio/mobil")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">Mobil</h1>
                  <h4 class="subtitulo">Activaciones Tornado de la suerte</h4>
                </div>
              </a>
            </div>
           
-->
<?php if (is_array($experimental)): ?>
  <?php foreach ($experimental as $key => $data): ?>
        <div class="banner" style="background-image: url('<?php echo base_url("assets/gallery/nuevas/$data->imagen_teaser"); ?>')">
           <a href="<?php echo site_url("inicio/experimental/$data->slug")  ?>">
            <div class="overlayinterior"></div>
            <div class="border">
              <h1 class="seccioninterior"> <?php echo $data->titulo; ?></h1>
              <h4 class="subtitulo"><?php echo $data->subtitulo; ?></h4>
            </div>
          </a>
        </div>
        <section class="section"></section>
  <?php endforeach ?>
<?php endif ?>


<div class="enlaceback"><a href="<?php echo site_url("inicio/services")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> our services </span></p></a>   <hr>  </div>
  