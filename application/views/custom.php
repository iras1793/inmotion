<!-- 
            <div class="banner" id="mars">
              <a href="<?php echo site_url("inicio/natgeomars")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">National Geographic</h1>
                  <h4 class="subtitulo">"Mars" Museo de historia natural</h4>
                </div>
              </a>
            </div>
             <section class="section"></section>
            <div class="banner" id="photoark">
             <a href="<?php echo site_url("inicio/natgeophotoark")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">National Geographic</h1>
                 <h4 class="subtitulo">Photo Ark</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
            <div class="banner" id="lgshowroom">
              <a href="<?php echo site_url("inicio/lgshowroom")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">LG</h1>
                  <h4 class="subtitulo">Showroom Business Solutions</h4>
                </div>
              </a>
            </div>
             <section class="section"></section>
            <div class="banner" id="trainingcenter">
              <a href="<?php echo site_url("inicio/bmwtrainingcenter")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">BMW</h1>
                  <h4 class="subtitulo"> Training center planta</h4>
                </div>
              </a>
            </div>
            <div class="banner" id="bmwmini">
              <a href="<?php echo site_url("inicio/bmwmini")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">BMW y MINI</h1>
                  <h4 class="subtitulo">Pop-up store</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>
            <div class="banner" id="minimotorrad">
              <a href="<?php echo site_url("inicio/bmwminimotorrad")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">BMW, MINI y MOTORRAD</h1>
                  <h4 class="subtitulo">Red de distribuidores</h4>
                </div>
              </a>
            </div>
           
             <section class="section"></section>
            <div class="banner" id="visitorcenter">
              <a href="<?php echo site_url("inicio/bmwvisitorcenter")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">BMW</h1>
                 <h4 class="subtitulo"> Visitor Center y Accesos principales planta</h4>
                </div>
              </a>
            </div>
            
            
            <section class="section"></section>
            <div class="banner" id="miniso">
             <a href="<?php echo site_url("inicio/minisoshowroom")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">Miniso</h1>
                  <h4 class="subtitulo">Showroom terraza</h4>
                </div>
              </a>
            </div>
           
 -->

<?php if (is_array($custom)): ?>
  <?php foreach ($custom as $key => $data): ?>
    
            <div class="banner" style="background-image: url('<?php echo base_url("assets/gallery/nuevas/$data->imagen_teaser"); ?>')">
              <a href="<?php echo site_url("inicio/custom/$data->slug")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"><?php echo $data->titulo ?></h1>
                  <h4 class="subtitulo">"<?php echo $data->subtitulo ?></h4>
                </div>
              </a>
            </div>
             <section class="section"></section>

  <?php endforeach ?>
<?php endif ?>


 <div class="enlaceback"><a href="<?php echo site_url("inicio/services")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> our services </span></p></a>   <hr>  </div>