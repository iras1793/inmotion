
            <div class="portada" id="headerlongines"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">FOX Sports</h1>
                  <h4 class="subtitulodos txtgray">Longines Global Champions Tour</h4>
                  <p class="txt2 txtgray">El Campo Marte en Ciudad de México fue el
escenario de la segunda etapa del Longines Global
Champions Tour y FOX Sports estuvo presente,
aprovechando el espacio, invitamos al público a
probar la app de FOX Sports a través de una
activación en la cual se regalaron 3 mil folios para
usarla gratis.
                  </p>
                  
              </div>
            </section>

            <!-- <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_2.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_6.jpg") ?>">
                        </li>
                         <li>
                         <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_7.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_8.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_9.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/longines/FOX_SPORTS_LONGINES_10.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

 <div class="enlaceback"><a href="<?php echo site_url("inicio/experientialmk")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Experiential MKT </span></p></a>   <hr> </div>