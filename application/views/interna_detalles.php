
<?php $fondo = $info[0]->imagen_fondo; ?>
<div class="portada" style="background: url('<?php echo base_url("assets/gallery/nuevas/$fondo"); ?>');
    background-position: center;
    background-size: cover;
    background-repeat: no-repeat">
  
</div>

  <section class="contenido_texto">
    <div class="contenido_parallax">
      <h1 class="seccioninterior txtgray"><?php echo $info[0]->titulo ?></h1>
      <h4 class="subtitulodos txtgray"><?php echo $info[0]->subtitulo ?></h4>
      <?php echo $info[0]->descripcion ?>
    </div>
  </section>

<?php if (!empty($info[0]->video)): ?>

    <?php if ( strstr($info[0]->video, 'http') || strstr($info[0]->video, 'https') ): ?>
          <?php 
          $video = explode("/", $info[0]->video); 
          $video_id = $video[3];
          ?>
          <div class="video">
            <div class="border">
                <iframe src="<?php echo "https://player.vimeo.com/video/$video_id" ?>" width="1280" height="720" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

            </div>
          </div>

          
    <?php else: ?>
          <div class="video">
            <div class="border">
              <?php $video = $info[0]->video;?>
              <?php $imagen = $info[0]->imagen_fondo; ?>
              <video controls="" muted="" preload="" poster='<?php echo base_url("assets/gallery/nuevas/$imagen") ?>'><source src='<?php echo base_url("assets/videos/$video") ?>'></video>
            </div>
          </div>
    <?php endif ?>

<?php endif ?>




  <section class="section"></section>

  <div class="galeria">
    <div class="items_galeria">
           <h4 class="subtitulodos txtgray">Galería</h4>
           <ul>
            <?php for($i=1; $i<=20; $i++): ?> 
              <?php $imagen='imagen_'.$i; ?>
              <?php if (!empty($info[0]->$imagen)): ?>
                <li>
                  <?php $imagen=$info[0]->$imagen; ?>
                  <img src="<?php echo base_url("assets/gallery/nuevas/$imagen") ?>" alt="<?php echo $imagen; ?>">
                </li>
              <?php endif ?>
            <?php endfor; ?>
          </ul> 

    </div>
  </div>

<?php if ($info[0]->seccion=='ExperimentalMarketing'): ?>
    <div class="enlaceback"><a href="<?php echo site_url("inicio/experimental")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Experimental Marketing</span></p></a>   <hr>  </div>
<?php else: ?>
    <?php $seccion=strtolower($info[0]->seccion); ?>
    <div class="enlaceback"><a href="<?php echo site_url("inicio/$seccion")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> <?php echo $info[0]->seccion ?> </span></p></a>   <hr>  </div>       
<?php endif ?>

