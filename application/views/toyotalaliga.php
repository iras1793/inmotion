
            <div class="portada" id="headertoyota"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">Toyota</h1>
                  <h4 class="subtitulodos txtgray">Conferencia de prensa Toyota - LaLiga</h4>
                  <p class="txt2 txtgray">Realizamos la conferencia de prensa en la que Toyota
en conjunto con LaLiga, dieron a conocer su alianza
2019-2021 en México y Latinoamérica.
                  </p>
                  <p class="txt2 txtgray">Contamos con el embajador de LaLiga Fernando
Morientes, ex-jugador del Real Madrid.</p>
                   
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
               <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa1.jpg") ?>">                    
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa2.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa7.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa8.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa9.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/toyota_laliga/toyota-laliga-2019-conferencia_prensa10.jpg") ?>">
                        </li>
                     </ul> 

              </div>
            </div>

 <div class="enlaceback"><a href="<?php echo site_url("inicio/events")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Events </span></p></a>   <hr>  </div>