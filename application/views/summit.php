
            <div class="portada" id="headersummit"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">Sports Antipiracy</h1>
                  <h4 class="subtitulodos txtgray">Summit 2019</h4>
                  <p class="txt2 txtgray">La primera edición realizada en el World Trade Center
de la ciudad de México el 20 y 21 de Febrero de 2019;
tuvo el impacto y la contundencia reflejada en la
asistencia en general, una altísima convocatoria de
prensa y protagonistas de nivel mundial.
                  </p>
              </div>
            </section>

            <div class="video">
              <div class="border">
                 <video controls muted preload poster="<?php echo base_url("assets/img/portada_videosummit.jpg") ?>"><source src="<?php echo base_url("assets/videos/Sport.mp4") ?>"></video>
              </div>
            </div>


            <section class="section"></section>

           <div class="galeria">
             <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit2.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit4.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/summit/summit5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit7.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit8.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/summit/summit9.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

 <div class="enlaceback"><a href="<?php echo site_url("inicio/events")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Events </span></p></a>   <hr>  </div>