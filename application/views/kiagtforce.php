
            <div class="portada" id="headerkia"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">KIA</h1>
                  <h4 class="subtitulodos txtgray">GT Family, prueba de manejo</h4>
                  <p class="txt2 txtgray">KIA presentó a prensa el grupo de autos que
conforman su “GT Family” en una experiencia de
prueba de manejo dentro del Autódromo Miguel E.
Abed en Amozoc, Puebla.
                  </p>
                  <p class="txt2 txtgray">Donde los asistentes pudieron manejar: FORTE
Hatchback, FORTE Sedán y Stinger en su versión GT.</p>
                   
              </div>
            </section>

            <!-- <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div>
 -->
            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO1.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO2.jpg") ?>">
                       </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO3.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO5.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/kia_gtforce/KIA_GT_FAMILY_2019_PRUEBA_MANEJO7.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/events")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Events </span></p></a>   <hr>  </div>