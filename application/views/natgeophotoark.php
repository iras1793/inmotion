
            <div class="portada" id="headernatgeophotoark"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">National Geographic</h1>
                <h4 class="subtitulodos txtgray">Photo Ark</h4>
                  <p class="txt2 txtgray">Montamos una exhibición itinerante con distintas
activaciones para chicos y grandes, que iban desde
reciclaje, hasta interacción con animales digitales.
                  </p>
                  <p class="txt2 txtgray">Creamos la experiencia Photo Ark de Joel Sartore en
Barrio Santa Fe.</p>
                   
              </div>
            </section>

          <div class="video">
              <div class="border">
                 <video controls muted preload poster="<?php echo base_url("assets/img/portada_videophotoark.jpg") ?>"><source src="<?php echo base_url("assets/videos/Ark.mp4") ?>"></video>
              </div>
            </div>


            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/photoark/photoark1.jpg") ?>">
                        </li>
                        <li>
                         <img src="<?php echo base_url("assets/gallery/photoark/photoark2.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/photoark/photoark3.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/photoark/photoark4.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/photoark/NATGEO_MARS_1.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

<div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
