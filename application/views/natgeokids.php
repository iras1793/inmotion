
            <div class="portada" id="headernatgeokids"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">NATGEO Kids</h1>
                  <h4 class="subtitulodos txtgray">NATGEO LAB, raro pero cierto </h4>
                  <p class="txt2 txtgray">Nat Geo Kids, presentó dos experiencias basadas en las series de “Nat Geo Lab” y “Raro pero
                  cierto”, montando un laboratorio en el que los niños tuvieron la oportunidad de crear
                  experimentos y un túnel-experiencia en el que aprendieron datos extraños.
                  </p>
                  <p class="txt2 txtgray">Las activaciones fueron dentro de plazas comerciales y NM estuvo a cargo de hacer laexperiencia realidad.</p>
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO1.jpg") ?>">
                        </li>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO2.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO3.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO4.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO5.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO6.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO7.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO8.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO9.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO10.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO11.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO12.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO13.jpg") ?>">
                        </li>
                         <li>
                          <img src="<?php echo base_url("assets/gallery/natgeo_kids/NATGEO_KIDS_RARO_PERO_CIERTO14.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

 <div class="enlaceback"><a href="<?php echo site_url("inicio/experientialmk")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Experiential MKT </span></p></a>   <hr> </div>