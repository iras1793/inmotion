<!-- 
             <div class="banner" id="tgp">
              <a href="<?php echo site_url("inicio/taboola")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"> Taboola</h1>
                  <h4 class="subtitulo">Glow Party</h4>
                </div>
              </a>
            </div>

            <section class="section"></section>
            
            <div class="banner" id="tb">
              <a href="<?php echo site_url("inicio/toryburch")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"> Tory Burch</h1>
                  <h4 class="subtitulo"> Primavera-Verano 2019</h4>
                </div>
              </a>
            </div>
             <section class="section"></section>
            <div class="banner" id="summit">
              <a href="<?php echo site_url("inicio/summit")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">Sports Anti-piracy</h1>
                  <h4 class="subtitulo">Summit 2019</h4>
                </div>
              </a>
            </div>
            <section class="section"></section> 
            <div class="banner" id="toyota">
              <a href="<?php echo site_url("inicio/toyotalaliga")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"> Toyota</h1>
                  <h4 class="subtitulo">Conferencia de prensa Toyota - LaLiga</h4>
                </div>
              </a>
            </div>               
            <section class="section"></section> 
            <div class="banner" id="kia">
              <a href="<?php echo site_url("inicio/kiagtforce")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">KIA</h1>
                  <h4 class="subtitulo"> GT Family prueba de manejo</h4>
                </div>
              </a>
            </div>
            <section class="section"></section>

            <div class="banner" id="equsport">
              <a href="<?php echo site_url("inicio/equsport")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior">Balvanera</h1>
                  <h4 class="subtitulo">Equsport 2019</h4>
                </div>
              </a>
            </div>
           

 -->

<?php if (is_array($eventos)): ?>
    <?php foreach ($eventos as $key => $data): ?>
          <div class="banner" style="background-image: url('<?php echo base_url("assets/gallery/nuevas/$data->imagen_teaser"); ?>')">
              <a href="<?php echo site_url("inicio/events/$data->slug")  ?>">
                <div class="overlayinterior"></div>
                <div class="border">
                  <h1 class="seccioninterior"> <?php echo $data->titulo; ?></h1>
                  <h4 class="subtitulo"><?php echo $data->subtitulo; ?></h4>
                </div>
              </a>
          </div>
          <section class="section"></section>
    <?php endforeach ?>
<?php endif ?>


        <div class="enlaceback"><a href="<?php echo site_url("inicio/services")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> our services </span></p></a>   <hr>  </div>

           
          
            
