
            <div class="portada" id="headertrainingcenter"></div>

            <section class="contenido_texto">
              <div class="contenido_parallax">
                  <h1 class="seccioninterior txtgray">BMW</h1>
                  <h4 class="subtitulodos txtgray">Training Center planta</h4>
                  <p class="txt2 txtgray">Montamos la señalética y la totalidad del Outdoor
Communication System junto con las astas y las
banderas de esta importante área dentro de la planta
armadora ubicada en San Luis Potosí.
                  </p>
                  <p class="txt2 txtgray">Esta planta es la más moderna de los 6 centros de
capacitación que BMW tiene a nivel mundial.</p>
                   
              </div>
            </section>

           <!--  <div class="video">
              <div class="border">
                <video src="videos/1.mp4" controls preload></video>
              </div>
            </div> -->

            <section class="section"></section>

            <div class="galeria">
              <div class="items_galeria">
                     <h4 class="subtitulodos txtgray">Galería</h4>
                     <ul>
                        <li>
                          <img src="<?php echo base_url("assets/gallery/bmw_trainingcenter/bmw_trainingcenter1.jpg") ?>">
                        </li>
                        <li>
                           <img src="<?php echo base_url("assets/gallery/bmw_trainingcenter/bmw_trainingcenter2.jpg") ?>">
                        </li>
                         <li>
                           <img src="<?php echo base_url("assets/gallery/bmw_trainingcenter/bmw_trainingcenter3.jpg") ?>">
                        </li>
                         <li>
                           <img src="<?php echo base_url("assets/gallery/bmw_trainingcenter/bmw_trainingcenter4.jpg") ?>">
                        </li>
                      </ul> 

              </div>
            </div>

  <div class="enlaceback"><a href="<?php echo site_url("inicio/custom")  ?>"><p class="txtgray"> Back to  <span class="txtorange"> Custom </span></p></a>   <hr>  </div>
