<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {


	var $data = array('pagina'=>'','title'=>'.:Inmotion:.','encabezado'=>'','miga_pan'=>'', 'redirect'=>'');
	var $tabla = 'casos_exito';
	var $id    = 'id';

	var $estilos = array(

	);

	var $javascript = array(

	);


	function __construct(){
		setcookie("inmotion-cookie" ,"Iras");
		parent::__construct();
		$this->general->table = 'configuraciones';
		$this->general->id    = 'id';

		$info = $this->general->get(1,array('status'=>1));

		$this->data['politicas'] = $info[0]->politicas;
		$this->data['cookies'] = $info[0]->cookies;
	}


	public function index(){
		$this->data['hideCookies'] = "false";
		
		$this->general->table = 'clientes';
		$this->general->id    = 'id';
		$this->data['clientes'] = $this->general->get(null, array('status'=>1), '*', '', true, null, null, 'asc');

		$this->general->table = 'keyplayers';
		$this->general->id    = 'id';
		$this->data['keyplayers'] = $this->general->get(null, array('status'=>1), '*', '', true, null, null, 'asc');

		$this->general->table = 'casos_exito';
		$this->general->id    = 'id';
		$this->data['casos_exito'] = $this->general->get(null, array('status'=>1, 'our_work'=>1));

		$this->template->content->view('inicio', $this->data);
		$this->data['redirect'] = site_url('inicio');
		$this->template->publish('template');
	}



	public function toryburch(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('toryburch', $this->data);
		$this->template->publish('template');
	}

	public function toyotalaliga(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('toyotalaliga', $this->data);
		$this->template->publish('template');
	}

	public function taboola(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('taboola', $this->data);
		$this->template->publish('template');
	}



	public function summit(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('summit', $this->data);
		$this->template->publish('template');
	}

	public function kiagtforce(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('kiagtforce', $this->data);
		$this->template->publish('template');
	}

	public function equsport(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('equsport', $this->data);
		$this->template->publish('template');
	}


/*	public function experientialmk(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		$this->data['redirect'] = site_url('inicio/experientialmk');
		$this->template->content->view('experientialmk', $this->data);
		$this->template->publish('template');
	}*/

	public function kiaformulam(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		$this->data['redirect'] = site_url('inicio/experientialmk');
		$this->template->content->view('kiaformulam', $this->data);
		$this->template->publish('template');
	}

	public function longines(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		$this->data['redirect'] = site_url('inicio/experientialmk');
		$this->template->content->view('longines', $this->data);
		$this->template->publish('template');
	}

	public function mobil(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		$this->data['redirect'] = site_url('inicio/experientialmk');
		$this->template->content->view('mobil', $this->data);
		$this->template->publish('template');
	}

	public function natgeokids(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		$this->data['redirect'] = site_url('inicio/experientialmk');
		$this->template->content->view('natgeokids', $this->data);
		$this->template->publish('template');
	}


	public function bmwmini(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('bmwmini', $this->data);
		$this->template->publish('template');
	}

	public function bmwminimotorrad(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('bmwminimotorrad', $this->data);
		$this->template->publish('template');
	}

	public function bmwtrainingcenter(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('bmwtrainingcenter', $this->data);
		$this->template->publish('template');
	}

	public function bmwvisitorcenter(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('bmwvisitorcenter', $this->data);
		$this->template->publish('template');
	}

	public function lgshowroom(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('lgshowroom', $this->data);
		$this->template->publish('template');
	}

	public function natgeomars(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('natgeomars', $this->data);
		$this->template->publish('template');
	}

	public function minisoshowroom(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('minisoshowroom', $this->data);
		$this->template->publish('template');
	}

	public function natgeophotoark(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('natgeophotoark', $this->data);
		$this->template->publish('template');
	}




	public function fox(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Branding';
		$this->data['redirect'] = site_url('inicio/branding');
		$this->template->content->view('fox', $this->data);
		$this->template->publish('template');
	}
	
	public function services(){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Services';
		$this->template->content->view('services', $this->data);
		$this->template->publish('template');
	}


	public function events($slug=''){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Events';
		
		
		if (!empty($slug)):
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$this->data['info'] = $this->general->get(null, array('status'=>1, 'slug'=>$slug), '*', '', true, null, "asc");
			$this->data['redirect'] = site_url('inicio/events');
			$this->template->content->view('interna_detalles', $this->data);
			$this->template->publish('template');
			return;
		endif;



		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['eventos']  = $this->general->get(null, array('status'=>1,'seccion'=>'Events'), '*', '', true, null, "asc");
		$this->data['redirect'] = site_url('inicio/events');
		$this->template->content->view('events', $this->data);
		$this->template->publish('template');
	}

	public function experimental($slug=''){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Experiential Marketing';
		
		
		if (!empty($slug)):
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$this->data['info'] = $this->general->get(null, array('status'=>1, 'slug'=>$slug), '*', '', true, null, "asc");
			$this->data['redirect'] = site_url('inicio/experimental');
			$this->template->content->view('interna_detalles', $this->data);
			$this->template->publish('template');
			return;
		endif;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['experimental'] = $this->general->get(null, array('status'=>1,'seccion'=>'ExperimentalMarketing'), '*', '', true, null, "asc");
		$this->data['redirect'] = site_url('inicio/experimental');
		$this->template->content->view('experientialmk', $this->data);
		$this->template->publish('template');
	}

	public function custom($slug=''){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Custom';

		if (!empty($slug)):
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$this->data['info'] = $this->general->get(null, array('status'=>1, 'slug'=>$slug), '*', '', true, null, "asc");
			$this->data['redirect'] = site_url('inicio/custom');
			$this->template->content->view('interna_detalles', $this->data);
			$this->template->publish('template');
			return;
		endif;

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['custom'] = $this->general->get(null, array('status'=>1,'seccion'=>'Custom'), '*', '', true, null, "asc");
		$this->data['redirect'] = site_url('inicio/custom');
		$this->template->content->view('custom', $this->data);
		$this->template->publish('template');
	}

	public function branding($slug=''){
		$this->data['hideCookies'] = (isset($_COOKIE['inmotion-cookie'])) ? true : false;
		$this->data['pagina'] = 'Branding';


		if (!empty($slug)):
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			$this->data['info'] = $this->general->get(null, array('status'=>1, 'slug'=>$slug), '*', '', true, null, "asc");
			$this->data['redirect'] = site_url('inicio/branding');
			$this->template->content->view('interna_detalles', $this->data);
			$this->template->publish('template');
			return;
		endif;		

		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		$this->data['branding'] = $this->general->get(null, array('status'=>1,'seccion'=>'Branding'), '*', '', true, null, "asc");
		$this->data['redirect'] = site_url('inicio/branding');
		$this->template->content->view('branding', $this->data);
		$this->template->publish('template');
	}


}
