<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	var $data = array('pagina'=>'dashboard','title'=>'.:Inmotion - Admin:.','encabezado'=>'','miga_pan'=>'Casos de éxito');
	var $tabla = 'casos_exito';
	var $id    = 'id';

	var $files_config = array(
		'upload_path'   => './assets/gallery/nuevas',
		'allowed_types' => 'gif|jpg|png|jpeg|svg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);

	var $files_config_keyplayers = array(
		'upload_path'   => './assets/gallery/keyplayers',
		'allowed_types' => 'gif|jpg|png|jpeg|svg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);

	var $files_config_clientes = array(
		'upload_path'   => './assets/gallery/clientes',
		'allowed_types' => 'gif|jpg|png|jpeg|svg',
		'max_size'      => 15000,//15MB
		'max_width'     => 0,
		'max_height'    => 0
	);


	var $fields = array(
			'titulo' => array('value'=>'','validate' => array(
											'label'   => 'Titulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'subtitulo' => array('value'=>'','validate' => array(
											'label'   => 'Subtitulo',
											'rules'   => 'trim|xss_clean|required'
										)),
			'descripcion' => array('value'=>'','validate' => array(
											'label'   => 'Descripción',
											'rules'   => 'trim'
										)),
			'imagen_teaser' => array('value'=>'','validate' => array(
											'label'   => 'Imagen Teaser',
											'rules'   => 'trim|xss_clean|callback_store[imagen_teaser]'
										)),
			'imagen_fondo' => array('value'=>'','validate' => array(
											'label'   => 'Imagen de Fondo',
											'rules'   => 'trim|xss_clean|callback_store[imagen_fondo]'
										)),
			'seccion' => array('value'=>'','validate' => array(
											'label'   => 'Seccion',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_1' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_1]'
										)),
			'imagen_2' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_2]'
										)),
			'imagen_3' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_3]'
										)),
			'imagen_4' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_4]'
										)),
			'imagen_5' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_5]'
										)),
			'imagen_6' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_6]'
										)),
			'imagen_7' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_7]'
										)),
			'imagen_8' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_8]'
										)),
			'imagen_9' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_9]'
										)),
			'imagen_10' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_10]'
										)),
			'imagen_11' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_11]'
										)),
			'imagen_12' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_12]'
										)),
			'imagen_13' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_13]'
										)),
			'imagen_14' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_14]'
										)),
			'imagen_15' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_15]'
										)),
			'imagen_16' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_16]'
										)),
			'imagen_17' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_17]'
										)),
			'imagen_18' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_18]'
										)),
			'imagen_19' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_19]'
										)),
			'imagen_20' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_20]'
										)),
			'our_work' => array('value'=>'','validate' => array(
											'label'   => 'Home',
											'rules'   => 'trim|xss_clean|required'
										)),
			'video' => array('value'=>'','validate' => array(
											'label'   => 'Video',
											'rules'   => 'trim'
										)),
	);
		

	var $estilos = array(
		'assets/admin/css/dataTables.min.css',
		'assets/admin/css/buttons.dataTables.min.css'
	);

	var $javascript = array(
		'assets/admin/js/app/dataTables/jquery.dataTables.min.js',
		'assets/admin/js/app/dataTables/dataTables.buttons.min.js',
		'assets/admin/js/app/dataTables/buttons.flash.min.js',
		'assets/admin/js/app/dataTables/jszip.min.js',
		'assets/admin/js/app/dataTables/pdfmake.js',
		'assets/admin/js/app/dataTables/vfs_fonts.js',
		'assets/admin/js/app/dataTables/buttons.html5.min.js',
		'assets/admin/js/app/dataTables/buttons.print.min.js'
	);

	function __construct(){
		parent::__construct();
		if( !$this->auth->loggedin() )
			redirect('login');
	}

	public function index(){

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		$this->general->table = 'casos_exito';
		$this->general->id = 'id';
		$this->data['casos'] = $this->general->get(null, array('status'=>1));

		$this->template->content->view('admin/dashboard', $this->data);
		$this->template->publish('admin/template_admin');
	}//end index function


	public function agregar($value=''){

		$this->data['miga_pan']   = 'Caso de éxito / Agregar';

		array_push($this->estilos, 'assets/admin/ckeditor/samples/css/samples.css');
		array_push($this->estilos, 'assets/admin/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css');

		array_push($this->javascript, 'assets/admin/ckeditor/ckeditor.js');
		array_push($this->javascript, 'assets/admin/ckeditor/samples/js/sample.js');
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;


		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ){
			//log_message("error","\n\nPasa validaciones\n\n");
			
			
			$this->general->table = $this->tabla;
			$this->general->id    = $this->id;
			
			if ($this->fields['imagen_fondo']['value']=='' || $this->fields['imagen_teaser']['value']==''):
				$this->messages->add("<strong>Por favor ingresa las imágenes para fondo y teaser</strong>","warning");
			else:
				$this->fields['slug']['value'] = _slug($this->fields['titulo']['value'] . "-" . $this->fields['subtitulo']['value']);
				$this->fields['descripcion']['value'] = str_replace("<h1>", "<h1 class='seccioninterior txtgray'>", $this->fields['descripcion']['value']);
				$success = $this->general->insert($this->fields);
				if ($success){
					$this->messages->add("<strong>Se agregado el registro con éxito.</strong>","success");
					foreach ($this->fields as $key => $value):
						$this->fields[$key]['value'] = '';
					endforeach;
				}
				else
					$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","danger");
			endif;
		}

		$this->data['id']     = '';
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'agregar';
		$this->template->content->view('admin/casos_form', $this->data);
		$this->template->publish('admin/template_admin');
	}

	public function editar($id=null){
		if (!is_numeric($id) || $id<=0 || $id==''):
			redirect('dashboard');
		endif;

		$this->data['miga_pan']   = 'Caso de éxito / Editar';

		array_push($this->estilos, 'assets/admin/ckeditor/samples/css/samples.css');
		array_push($this->estilos, 'assets/admin/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css');

		array_push($this->javascript, 'assets/admin/ckeditor/ckeditor.js');
		array_push($this->javascript, 'assets/admin/ckeditor/samples/js/sample.js');
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;
		
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;

		//comienza logica
		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");

			if ($this->fields['imagen_fondo']['value']==''):
				unset($this->fields['imagen_fondo']);
			endif;

			if ($this->fields['imagen_teaser']['value']==''):
				unset($this->fields['imagen_teaser']);
			endif;

			for ($i=1; $i <=20 ; $i++):
				$eliminar = "eliminar_".$i;
				$imagen   = 'imagen_'.$i;
				
				if( empty($this->fields[$imagen]['value']) && !array_key_exists($eliminar, $_POST) ):
					unset($this->fields[$imagen]);
				
				elseif(array_key_exists($eliminar, $_POST)):
					$this->fields[$imagen]['value'] = '';
				endif;

			endfor;

			$this->fields['slug']['value'] = _slug($this->fields['titulo']['value'] . "-" . $this->fields['subtitulo']['value']);
			$this->fields['descripcion']['value'] = str_replace("<h1>", "<h1 class='seccioninterior txtgray'>", $this->fields['descripcion']['value']);
			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Se a editado el registro con éxito.</strong>","success");
			else:
				$this->messages->add("<strong>Hubo un error al actualizar el registro, por favor verifica que hayas ingresado los campos obligatorios</strong>","danger");
			endif;
		endif;
		//termina logica
		
		$info = $this->general->get($id, array('status'=>1));
		$this->fields['imagen_teaser']['value'] ='';
		$this->fields['imagen_fondo']['value']  ='';
		for ($i=1; $i <=20 ; $i++):
			$imagen = "imagen_".$i;
			$this->fields[$imagen]['value'] = '';
		endfor;
		if (is_array($info)):
			foreach ($this->fields as $key => $value):
				$this->fields[$key]['value'] = $info[0]->$key;
			endforeach;
		else:
			$this->messages->add("<strong>No existe el registro que deseas editar</strong>","danger");
		endif;

		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'editar';
		$this->template->content->view('admin/casos_form', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function eliminar($id=null){
		if( is_null($id) || !is_numeric($id) || $id <= 0 || $id==''):
			redirect("dashboard");
		endif;

		$this->messages->clear();
		$this->general->table = $this->tabla;
		$this->general->id    = $this->id;
		
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>Se a eliminado el registro con éxito</strong>","success");
		else:
			$this->messages->add("<strong>Se detectó un error, favor de intentarlo más tarde</strong>","danger");
		endif;
		
		redirect("dashboard");
	}


	public function politicas($id=''){

		$this->data['miga_pan']   = 'Políticas de Privacidad';

		array_push($this->estilos, 'assets/admin/ckeditor/samples/css/samples.css');
		array_push($this->estilos, 'assets/admin/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css');

		array_push($this->javascript, 'assets/admin/ckeditor/ckeditor.js');
		array_push($this->javascript, 'assets/admin/ckeditor/samples/js/sample.js');
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;
		$this->data['pagina']   = 'politicas';

		$this->general->table = 'configuraciones';
		$this->general->id = 'id';
		

		//comienza logica
		$this->messages->clear();
		$this->form_validation->set_rules('politicas', 'Politicas', 'required');
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");
			$campos['politicas']['value'] = $this->input->post('politicas');
			$campos['fecha']['value'] = date("Y-m-d H-i-s");
			$success = $this->general->update($campos, 1);
			if ($success):
				$this->messages->add("<strong>Se a editado el registro con éxito.</strong>","success");
			else:
				$this->messages->add("<strong>Hubo un error al actualizar el registro, por favor verifica que hayas ingresado los campos obligatorios</strong>","danger");
			endif;
		endif;
		//termina logica
		
		$info = $this->general->get($id, array('status'=>1));
		if (is_array($info)):
			$campos['politicas']['value'] = $info[0]->politicas;
		else:
			$this->messages->add("<strong>No existe el registro que deseas editar</strong>","danger");
		endif;

		$this->data['id']     = 1;
		$this->data['fields'] = $campos;
		$this->template->content->view('admin/politicas', $this->data);
		$this->template->publish('admin/template_admin');
	}

	public function cookies($id=''){
		$this->data['miga_pan']   = 'Cookies';
		array_push($this->estilos, 'assets/admin/ckeditor/samples/css/samples.css');
		array_push($this->estilos, 'assets/admin/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css');

		array_push($this->javascript, 'assets/admin/ckeditor/ckeditor.js');
		array_push($this->javascript, 'assets/admin/ckeditor/samples/js/sample.js');
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;
		$this->data['pagina']   = 'cookies';

		$this->general->table = 'configuraciones';
		$this->general->id = 'id';
		

		//comienza logica
		$this->messages->clear();
		$this->form_validation->set_rules('cookies', 'Politicas', 'required');
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");
			$campos['cookies']['value'] = $this->input->post('cookies');
			$campos['fecha']['value'] = date("Y-m-d H-i-s");
			$success = $this->general->update($campos, 1);
			if ($success):
				$this->messages->add("<strong>Se a editado el registro con éxito.</strong>","success");
			else:
				$this->messages->add("<strong>Hubo un error al actualizar el registro, por favor verifica que hayas ingresado los campos obligatorios</strong>","danger");
			endif;
		endif;
		//termina logica
		
		$info = $this->general->get($id, array('status'=>1));
		if (is_array($info)):
			$campos['cookies']['value'] = $info[0]->cookies;
		else:
			$this->messages->add("<strong>No existe el registro que deseas editar</strong>","danger");
		endif;

		$this->data['id']     = 1;
		$this->data['fields'] = $campos;
		$this->template->content->view('admin/cookies', $this->data);
		$this->template->publish('admin/template_admin');
	}



	public function keyplayers(){
		$this->data['pagina']   = 'keyplayers';
		$this->data['miga_pan'] = 'Keyplayers';
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = 'keyplayers';
		$this->general->id    = 'id';
		$this->data['keyplayers'] = $this->general->get(null, array('status'=>1));

		$this->template->content->view('admin/keyplayers', $this->data);
		$this->template->publish('admin/template_admin');
	}//end index function




	public function agregar_keyplayers($id=''){
		$this->data['pagina']   = 'keyplayers';
		$this->data['miga_pan']   = 'Keyplayers / Agregar';
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//comienza logica
		$this->fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Nombre',
											'rules'   => 'trim|xss_clean|required'
										)),
			'cargo' => array('value'=>'','validate' => array(
											'label'   => 'Cargo / Puesto',
											'rules'   => 'trim|xss_clean|required'
										)),
			'linkedin' => array('value'=>'','validate' => array(
											'label'   => 'Linkedin',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_keyplayer' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_keyplayer]'
										))
		);

		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ){
			//log_message("error","\n\nPasa validaciones\n\n");
			
			$this->general->table = 'keyplayers';
			$this->general->id    = 'id';
			
			if ($this->fields['imagen_keyplayer']['value']==''):
				$this->messages->add("<strong>Por favor ingresa la imagen</strong>","danger");
			else:
				$success = $this->general->insert($this->fields);
				if ($success){
					$this->messages->add("<strong>Se agregado el registro con éxito.</strong>","success");
					foreach ($this->fields as $key => $value):
						$this->fields[$key]['value'] = '';
					endforeach;
				}
				else
					$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","danger");
			endif;
		}

		$this->data['id']     = '';
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'agregar_keyplayers';
		$this->template->content->view('admin/keyplayers_form', $this->data);
		$this->template->publish('admin/template_admin');
	}




	public function editar_keyplayers($id=null){
		if (!is_numeric($id) || $id<=0 || $id==''):
			redirect('dashboard/keyplayers');
		endif;

		$this->data['miga_pan']   = 'Keyplayers / Editar';
		$this->data['pagina']   = 'keyplayers';
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;
		
		$this->general->table = 'keyplayers';
		$this->general->id    = 'id';

		//comienza logica
		$this->fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Nombre',
											'rules'   => 'trim|xss_clean|required'
										)),
			'cargo' => array('value'=>'','validate' => array(
											'label'   => 'Cargo / Puesto',
											'rules'   => 'trim|xss_clean|required'
										)),
			'linkedin' => array('value'=>'','validate' => array(
											'label'   => 'Linkedin',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_keyplayer' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_keyplayer]'
										))
		);

		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");

			if ($this->fields['imagen_keyplayer']['value']==''):
				unset($this->fields['imagen_keyplayer']);
			endif;

			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Se a editado el registro con éxito.</strong>","success");
			else:
				$this->messages->add("<strong>Hubo un error al actualizar el registro, por favor verifica que hayas ingresado los campos obligatorios</strong>","danger");
			endif;
		endif;
		//termina logica
		
		$info = $this->general->get($id, array('status'=>1));
		$this->fields['imagen_keyplayer']['value'] ='';
		if (is_array($info)):
			foreach ($this->fields as $key => $value):
				$this->fields[$key]['value'] = $info[0]->$key;
			endforeach;
		else:
			$this->messages->add("<strong>No existe el registro que deseas editar</strong>","danger");
		endif;

		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'editar_keyplayers';
		$this->template->content->view('admin/keyplayers_form', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function eliminar_keyplayers($id=null){
		if( is_null($id) || !is_numeric($id) || $id <= 0 || $id==''):
			redirect("dashboard/heyplayers");
		endif;

		$this->messages->clear();
		$this->general->table = 'keyplayers';
		$this->general->id    = 'id';
		
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>Se a eliminado el registro con éxito</strong>","success");
		else:
			$this->messages->add("<strong>Se detectó un error, favor de intentarlo más tarde</strong>","danger");
		endif;
		
		redirect("dashboard/keyplayers");
	}


	public function clientes(){
		$this->data['pagina']   = 'clientes';
		$this->data['miga_pan'] = 'Clientes';
		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;

		$this->general->table = 'clientes';
		$this->general->id    = 'id';
		$this->data['clientes'] = $this->general->get(null, array('status'=>1));

		$this->template->content->view('admin/clientes', $this->data);
		$this->template->publish('admin/template_admin');
	}//end index function

	public function agregar_cliente($id=''){
		$this->data['pagina']   = 'keyplayers';
		$this->data['miga_pan']   = 'Clientes / Agregar';
		
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos'] = $this->estilos;

		//comienza logica
		$this->fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Nombre',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_cliente' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_cliente]'
										))
		);

		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ){
			//log_message("error","\n\nPasa validaciones\n\n");
			
			$this->general->table = 'clientes';
			$this->general->id    = 'id';
			
			if ($this->fields['imagen_cliente']['value']==''):
				$this->messages->add("<strong>Por favor ingresa la imagen</strong>","danger");
			else:
				$success = $this->general->insert($this->fields);
				if ($success){
					$this->messages->add("<strong>Se agregado el registro con éxito.</strong>","success");
					foreach ($this->fields as $key => $value):
						$this->fields[$key]['value'] = '';
					endforeach;
				}
				else
					$this->messages->add("<strong>Por favor llena los campos obligatorios</strong>","danger");
			endif;
		}

		$this->data['id']     = '';
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'agregar_cliente';
		$this->template->content->view('admin/clientes_form', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function editar_clientes($id=null){
		if (!is_numeric($id) || $id<=0 || $id==''):
			redirect('dashboard/clientes');
		endif;

		$this->data['miga_pan']   = 'Clientes / Editar';
		$this->data['pagina']   = 'clientes';
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;
		
		$this->general->table = 'clientes';
		$this->general->id    = 'id';

		//comienza logica
		$this->fields = array(
			'nombre' => array('value'=>'','validate' => array(
											'label'   => 'Nombre',
											'rules'   => 'trim|xss_clean|required'
										)),
			'imagen_cliente' => array('value'=>'','validate' => array(
											'label'   => 'Imagen',
											'rules'   => 'trim|xss_clean|callback_store[imagen_cliente]'
										))
		);

		$this->messages->clear();
		$this->config_validates();
		if( $this->form_validation->run() != FALSE ):
			//log_message("error","\n\nPasa validaciones\n\n");

			if ($this->fields['imagen_cliente']['value']==''):
				unset($this->fields['imagen_cliente']);
			endif;

			$success = $this->general->update($this->fields, $id);
			if ($success):
				$this->messages->add("<strong>Se a editado el registro con éxito.</strong>","success");
			else:
				$this->messages->add("<strong>Hubo un error al actualizar el registro, por favor verifica que hayas ingresado los campos obligatorios</strong>","danger");
			endif;
		endif;
		//termina logica
		
		$info = $this->general->get($id, array('status'=>1));
		$this->fields['imagen_cliente']['value'] ='';
		if (is_array($info)):
			foreach ($this->fields as $key => $value):
				$this->fields[$key]['value'] = $info[0]->$key;
			endforeach;
		else:
			$this->messages->add("<strong>No existe el registro que deseas editar</strong>","danger");
		endif;

		$this->data['id']     = $id;
		$this->data['fields'] = $this->fields;
		$this->data['action'] = 'editar_clientes';
		$this->template->content->view('admin/clientes_form', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function eliminar_cliente($id=null){
		if( is_null($id) || !is_numeric($id) || $id <= 0 || $id==''):
			redirect("dashboard/clientes");
		endif;

		$this->messages->clear();
		$this->general->table = 'clientes';
		$this->general->id    = 'id';
		
		$campos['status']['value'] = 0;
		$success = $this->general->update($campos, $id);
		if ($success):
			$this->messages->add("<strong>Se a eliminado el registro con éxito</strong>","success");
		else:
			$this->messages->add("<strong>Se detectó un error, favor de intentarlo más tarde</strong>","danger");
		endif;
		
		redirect("dashboard/clientes");
	}


	public function ordenar_keyplayers(){

		$this->data['miga_pan']   = 'Ordenar Keyplayers';
		$this->data['pagina']   = 'keyplayers';
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;        
        
        $this->general->table = 'keyplayers';
        $this->general->id    = 'id';
        $registros = $this->general->get(null, array('status'=>1));
        /*comienza logica*/
        $this->data['totales'] = count($registros);
        $this->messages->clear();
        for($i=0; $i<$this->data['totales']; $i++):
            $this->form_validation->set_rules('orden_'.$registros[$i]->id, 'Orden '.$registros[$i]->id, 'trim|required|xss_clean');
        endfor;
        
        if( $this->form_validation->run() != FALSE ):
            $this->general->table = 'keyplayers';
            $this->general->id    = 'id';
            
            
            for($i=0; $i<$this->data['totales']; $i++):
                $campos['orden']['value'] = $this->input->post('orden_'.$registros[$i]->id, true);
                $success = $this->general->update($campos, $registros[$i]->id);

            endfor;
            if ($success):
            	$this->messages->add("<strong>Ordenes establecidos</strong>","success");
            else:
            	$this->messages->add("<strong>Hubo un error al establecer los ordenes</strong>","danger");
            endif;
        endif;
        $registros = $this->general->get(null, array('status'=>1));
        $this->data['registros'] = $registros;

		$this->template->content->view('admin/keyplayers_orden', $this->data);
		$this->template->publish('admin/template_admin');
	}


	public function ordenar_clientes(){

		$this->data['miga_pan']   = 'Ordenar Clientes';
		$this->data['pagina']   = 'clientes';
		array_push($this->javascript, 'assets/admin/js/jquery.validate.js');
		array_push($this->javascript, 'assets/admin/js/additional-methods.min.js');
		

		$this->data['javascript'] = $this->javascript;
		$this->data['estilos']    = $this->estilos;        
        
        $this->general->table = 'clientes';
        $this->general->id    = 'id';
        $registros = $this->general->get(null, array('status'=>1));
        /*comienza logica*/
        $this->data['totales'] = count($registros);
        $this->messages->clear();
        for($i=0; $i<$this->data['totales']; $i++):
            $this->form_validation->set_rules('orden_'.$registros[$i]->id, 'Orden '.$registros[$i]->id, 'trim|required|xss_clean');
        endfor;
        
        if( $this->form_validation->run() != FALSE ):
            $this->general->table = 'clientes';
            $this->general->id    = 'id';
            
            for($i=0; $i<$this->data['totales']; $i++):
                $campos['orden']['value'] = $this->input->post('orden_'.$registros[$i]->id, true);
                $success = $this->general->update($campos, $registros[$i]->id);

            endfor;
            if ($success):
            	$this->messages->add("<strong>Ordenes establecidos</strong>","success");
            else:
            	$this->messages->add("<strong>Hubo un error al establecer los ordenes</strong>","danger");
            endif;
        endif;
        $registros = $this->general->get(null, array('status'=>1));
        $this->data['registros'] = $registros;

		$this->template->content->view('admin/clientes_orden', $this->data);
		$this->template->publish('admin/template_admin');
	}



	public function store($value,$source){
        $ext = pathinfo($_FILES[$source]["name"], PATHINFO_EXTENSION);
        
        if ($source == 'imagen_keyplayer'):
        	$this->upload->initialize($this->files_config_keyplayers);
        elseif($source == 'imagen_cliente'):
			$this->upload->initialize($this->files_config_clientes);
        else:
        	$this->upload->initialize($this->files_config);	
        endif;

        log_message("error","\n\nVeamos source: $source\n\n");
        if( isset($_FILES) ):
	        $ext = pathinfo($_FILES[$source]["name"], PATHINFO_EXTENSION);
	        $nuevo_nombre = "thumb_".date('Y-m-d-H-i-s').".".$ext;
	        $_FILES[$source]["name"] = $nuevo_nombre;
	        if (!$this->upload->do_upload($source)):
	            $error = array('error' => $this->upload->display_errors());
	            log_message("error","\n\nError al subir imagen =====> ".json_encode($error)."\n\n");
	        else:
	            $data = array('image_metadata' => $this->upload->data());
	        	$this->fields[$source]['value'] = $data['image_metadata']['file_name'];
	        endif;
        endif;
	}


	/**
	 * Funcion privada que ayuda a validar campos
	 */
	private function config_validates(){
		$config = array();
		foreach( $this->fields as $key=>$field ){
			$field['validate']['field'] = $key;
			$config[] = $field['validate'];
		}
		$this->form_validation->set_rules($config);
		foreach( $this->fields as $key=>$field )
			$this->fields[$key]['value'] = $this->input->post($key);
	}//end config_validates



}//end class
