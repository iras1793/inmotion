-- MySQL dump 10.17  Distrib 10.3.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: inmotion
-- ------------------------------------------------------
-- Server version	10.3.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `casos_exito`
--

DROP TABLE IF EXISTS `casos_exito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casos_exito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) COLLATE utf8_bin DEFAULT '',
  `subtitulo` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_teaser` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_fondo` varchar(100) COLLATE utf8_bin DEFAULT '',
  `descripcion` text COLLATE utf8_bin DEFAULT NULL,
  `seccion` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_1` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_2` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_3` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_4` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_5` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_6` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_7` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_8` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_9` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_10` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_11` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_12` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_13` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_14` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_15` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_16` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_17` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_18` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_19` varchar(100) COLLATE utf8_bin DEFAULT '',
  `imagen_20` varchar(100) COLLATE utf8_bin DEFAULT '',
  `fecha` datetime DEFAULT current_timestamp(),
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `casos_exito`
--

LOCK TABLES `casos_exito` WRITE;
/*!40000 ALTER TABLE `casos_exito` DISABLE KEYS */;
INSERT INTO `casos_exito` VALUES (1,'test1','subtes1','asdasd','asdasd','','','','','','','','','','','','','','','','','','','','','','','2019-12-10 01:49:02',0),(2,'test2','subtes1','asdasd','asdasd','','','','','','','','','','','','','','','','','','','','','','','2019-12-10 01:49:02',0),(3,'test3','subtes1','asdasd','asdasd','','','','','','','','','','','','','','','','','','','','','','','2019-12-10 01:49:02',0),(4,'test4','subtes1','asdasd','asdasd','','','','','','','','','','','','','','','','','','','','','','','2019-12-10 01:49:02',0),(5,'test5','subtes1','asdasd','asdasd','','','','','','','','','','','','','','','','','','','','','','','2019-12-10 01:49:02',0),(6,'jhkjhkjhkjh','kjhkjhkj','thumb_2019-12-11-04-33-59.png','thumb_2019-12-11-04-33-591.png','<p>asdasdasd</p>','Custom','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:33:59',1),(7,'sdffdgdfgd','xcdfg','thumb_2019-12-11-04-35-13.jpg','thumb_2019-12-11-04-35-131.jpg','<p>zxczxc</p>','Events','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:35:13',1),(8,'asdasd','asdasd','thumb_2019-12-11-04-38-23.png','thumb_2019-12-11-04-38-23.jpg','','Branding','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:38:23',1),(9,'asdasd','asdasd','thumb_2019-12-11-04-43-14.jpg','thumb_2019-12-11-04-43-141.jpg','','Events','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:43:14',1),(10,'ghjghjg','ghjghj','thumb_2019-12-11-04-44-54.jpg','thumb_2019-12-11-04-44-541.jpg','<p><span xss=removed><span xss=removed><span xss=removed>ghjghjghjghjghj</span></span></span></p>','ExperimentalMarketing','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:44:54',1),(11,'cbgh','ghjugm','thumb_2019-12-11-04-45-57.jpg','thumb_2019-12-11-04-45-571.jpg','<p><span xss=removed><span xss=removed><span xss=removed>vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv</span></span></span></p>','Custom','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:45:57',1),(12,'xxx','xxx','thumb_2019-12-11-04-48-01.jpg','thumb_2019-12-11-04-48-011.jpg','<p><span xss=removed><span xss=removed>adsxx</span></span></p>','Events','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:48:01',1),(13,'asdasdasd','scd','thumb_2019-12-11-04-49-04.jpg','thumb_2019-12-11-04-49-041.jpg','<p><span style=\"color:#c0392b\"><span style=\"font-size:28px\"><span style=\"font-family:Comic Sans MS,cursive\">zxczxczxc</span></span></span></p>','Custom','','','','','','','','','','','','','','','','','','','','','2019-12-11 04:49:04',1),(14,'PowerWolf','Buena música','thumb_2019-12-18-01-10-41.png','thumb_2019-12-18-01-10-41.jpg','<p><span style=\"font-family:Comic Sans MS,cursive\"><span style=\"font-size:24px\"><span style=\"color:#4e5f70\">Est&aacute; bien buena esa vieja</span></span></span></p>','Events','','','','','','','','','','','','','','','','','','','','','2019-12-18 01:10:41',1),(15,'Ave edit','María edit','thumb_2019-12-18-01-15-23.png','thumb_2019-12-18-01-15-23.jpg','<p><span style=\"color:#c0392b\"><span style=\"font-family:Georgia,serif\"><span style=\"font-size:26px\">AVE MARIA edit</span></span></span></p>','Events','','','','','','','','','','','','','','','','','','','','','2019-12-18 01:15:23',1),(16,'asdasd','sdxv','thumb_2019-12-18-02-51-06.png','thumb_2019-12-18-02-51-061.png','<p>sdcsdc</p>','Branding','thumb_2019-12-18-02-51-06.jpg','thumb_2019-12-18-02-51-062.png','thumb_2019-12-18-02-51-063.png','','','','','','','','','','','','','','','','','','2019-12-18 02:51:06',1);
/*!40000 ALTER TABLE `casos_exito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuraciones`
--

DROP TABLE IF EXISTS `configuraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `politicas` longtext COLLATE utf8_bin DEFAULT NULL,
  `cookies` longtext COLLATE utf8_bin DEFAULT NULL,
  `fecha` datetime DEFAULT current_timestamp(),
  `status` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuraciones`
--

LOCK TABLES `configuraciones` WRITE;
/*!40000 ALTER TABLE `configuraciones` DISABLE KEYS */;
INSERT INTO `configuraciones` VALUES (1,'<p>El presente Pol&iacute;tica de Privacidad establece los t&eacute;rminos en que CUADRO NARANJA COMUNICACION usa y protege la informaci&oacute;n que es proporcionada por sus usuarios al momento de utilizar su<br />sitio web. Esta compa&ntilde;&iacute;a est&aacute; comprometida con la seguridad de los datos de sus usuarios. Cuando le pedimos llenar los campos de informaci&oacute;n personal con la cual usted pueda ser identificado, lo<br />hacemos asegurando que s&oacute;lo se emplear&aacute; de acuerdo con los t&eacute;rminos de este documento. Sin embargo esta Pol&iacute;tica de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le<br />recomendamos y enfatizamos revisar continuamente esta p&aacute;gina para asegurarse que est&aacute; de acuerdo con dichos cambios.</p><p><strong>Informaci&oacute;n que es recogida</strong><br />Nuestro sitio web podr&aacute; recoger informaci&oacute;n personal por ejemplo: Nombre, informaci&oacute;n de contacto como su direcci&oacute;n de correo electr&oacute;nica e informaci&oacute;n demogr&aacute;fica. As&iacute; mismo cuando sea<br />necesario podr&aacute; ser requerida informaci&oacute;n espec&iacute;fica para procesar alg&uacute;n pedido o realizar una entrega o facturaci&oacute;n.</p><p><strong>Uso de la informaci&oacute;n recogida</strong><br />Nuestro sitio web emplea la informaci&oacute;n con el fin de proporcionar el mejor servicio posible, particularmente para mantener un registro de usuarios, de pedidos en caso que aplique, y mejorar nuestros<br />productos y servicios. Es posible que sean enviados correos electr&oacute;nicos peri&oacute;dicamente a trav&eacute;s de nuestro sitio con ofertas especiales, nuevos productos y otra informaci&oacute;n publicitaria que<br />consideremos relevante para usted o que pueda brindarle alg&uacute;n beneficio, estos correos electr&oacute;nicos ser&aacute;n enviados a la direcci&oacute;n que usted proporcione y podr&aacute;n ser cancelados en cualquier<br />momento.</p><p>CUADRO NARANJA COMUNICACION est&aacute; altamente comprometido para cumplir con el compromiso de mantener su informaci&oacute;n segura. Usamos los sistemas m&aacute;s avanzados y los actualizamos<br />constantemente para asegurarnos que no exista ning&uacute;n acceso no autorizado.</p><p><strong>Cookies</strong><br />Una cookie se refiere a un fichero que es enviado con la finalidad de solicitar permiso para almacenarse en su ordenador, al aceptar dicho fichero se crea y la cookie sirve entonces para tener<br />informaci&oacute;n respecto al tr&aacute;fico web, y tambi&eacute;n facilita las futuras visitas a una web recurrente. Otra funci&oacute;n que tienen las cookies es que con ellas las web pueden reconocerte individualmente y por<br />tanto brindarte el mejor servicio personalizado de su web.</p><p>Nuestro sitio web emplea las cookies para poder identificar las p&aacute;ginas que son visitadas y su frecuencia. Esta informaci&oacute;n es empleada &uacute;nicamente para an&aacute;lisis estad&iacute;stico y despu&eacute;s la informaci&oacute;n<br />se elimina de forma permanente. Usted puede eliminar las cookies en cualquier momento desde su ordenador. Sin embargo las cookies ayudan a proporcionar un mejor servicio de los sitios web,<br />est&aacute;s no dan acceso a informaci&oacute;n de su ordenador ni de usted, a menos de que usted as&iacute; lo quiera y la proporcione directamente, visitas a una web . Usted puede aceptar o negar el uso de cookies,<br />sin embargo la mayor&iacute;a de navegadores aceptan cookies autom&aacute;ticamente pues sirve para tener un mejor servicio web. Tambi&eacute;n usted puede cambiar la configuraci&oacute;n de su ordenador para declinar<br />las cookies. Si se declinan es posible que no pueda utilizar algunos de nuestros servicios.<br />&nbsp;</p><p><strong>Enlaces a Terceros</strong><br />Este sitio web pudiera contener en laces a otros sitios que pudieran ser de su inter&eacute;s. Una vez que usted de clic en estos enlaces y abandone nuestra p&aacute;gina, ya no tenemos control sobre al sitio al<br />que es redirigido y por lo tanto no somos responsables de los t&eacute;rminos o privacidad ni de la protecci&oacute;n de sus datos en esos otros sitios terceros. Dichos sitios est&aacute;n sujetos a sus propias pol&iacute;ticas de<br />privacidad por lo cual es recomendable que los consulte para confirmar que usted est&aacute; de acuerdo con estas.</p><p><strong>Control de su informaci&oacute;n personal</strong><br />En cualquier momento usted puede restringir la recopilaci&oacute;n o el uso de la informaci&oacute;n personal que es proporcionada a nuestro sitio web. Cada vez que se le solicite rellenar un formulario, como el<br />de alta de usuario, puede marcar o desmarcar la opci&oacute;n de recibir informaci&oacute;n por correo electr&oacute;nico. En caso de que haya marcado la opci&oacute;n de recibir nuestro bolet&iacute;n o publicidad usted puede<br />cancelarla en cualquier momento.</p><p><br />Esta compa&ntilde;&iacute;a no vender&aacute;, ceder&aacute; ni distribuir&aacute; la informaci&oacute;n personal que es recopilada sin su consentimiento, salvo que sea requerido por un juez con un orden judicial.</p><p><br />CUADRO NARANJA COMUNICACION Se reserva el derecho de cambiar los t&eacute;rminos de la presente Pol&iacute;tica de Privacidad en cualquier momento.</p>','<p>Uso de cookies, utilizamos cookies para obtener informaci&oacute;n estad&iacute;stica sobre quienes nos visitan. Al navegar en este sitio aceptas su uso.</p><p>Para m&aacute;s informaci&oacute;n consulta nuestra<a class=\"enlacePrivacidad\" href=\"#modalprivacidad\"> Pol&iacute;tica de Privacidad</a></p>','2019-12-24 01:16:39',1);
/*!40000 ALTER TABLE `configuraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin DEFAULT '',
  `pwd` varchar(100) COLLATE utf8_bin DEFAULT '',
  `fecha` datetime DEFAULT current_timestamp(),
  `status` int(11) DEFAULT 1,
  `username` varchar(100) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'vcruz@gmail.com','202cb962ac59075b964b07152d234b70','2019-12-10 01:14:00',1,'vcruz'),(2,'agarcia@inmotion.com','202cb962ac59075b964b07152d234b70','2019-12-10 01:14:00',1,'Anaid');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'inmotion'
--
